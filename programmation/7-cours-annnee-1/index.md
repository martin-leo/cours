# index des cours

Objectif : donner des foncdamentaux généralement mis de côté sur ce qu’on peut trouver le plus facilement sur le net : on ne va pas refaire ce qui est facilement accessible avec internet, on va plutôt essayer de voir ce qui iest fondamental et plus facile à rater.

En gros : donner aux étudiants le « big picture » et les infos importantes qui sont facilement oubliées ailleurs. qu’ils aient une idée générale et puisse s’autoformer en aillant de bonnes informations pour le faire.

* 1. notions générales de programmation
  * Comment l’ordinateur traite-t-il un programme ?
    * processus
    * lexique
  * concepts et paradigmes
  * introduction rapide HTML/CSS/JavaScript en vue des prochains cours
* 2. ce qu’il faut savoir sur… HTML
  * très très bref historique
  * encapsuler de l’information avec de la sémantique. HTML -> Structure sémantique
  * balises, attributs, DOM, page outline
  * accessibilité
  * bonnes pratiques
  * living standard
  * HTML5 « core » et web APIs
    * websocket
    * canvas
    * getusermedia
    * webRTC
    * web workers ?
    * XMLHTTPRequest
    * Drag and Drop
    * touch events
    * webGL
  * ce qu’on fait habituellement quand on utilise du HTML
  * où trouver de l’information
    * MDN, W3C, whatWG, validators, sitepoint
  * bonus : le SVG
* 3. ce qu’il faut savoir sur… CSS
  * sélecteurs, mécanisme de sélection (de droite à gauche)
  * propriétés
  * mise en page
  * design adaptatif : les media-queries
  * living standard
  * bonnes pratiques
  * ce qu’on fait habituellement quand on utilise du CSS
  * où trouver de l’information
    * MDN, W3C, whatWG, validators, caniuse, sitepoint
  * bonus : les préprocesseurs
* 4. ce qu’il faut savoir, JavaScript ?
  * langage : les mots clés du langage
  * environnement hôte
  * accéder au HTML
  * accéder au CSS
  * où trouver de l’information
    * MDN, W3C, whatWG, validators, caniuse, sitepoint
* 5. programmation séquentielle
* 6. programmation procédurale et événementielle ?
* 7. programmation orientée objet
