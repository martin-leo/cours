# Notes relatives à la présentation du cours.

* Il pourrait être plus judicieux, lors de l’introduction à la notion d’algorithme, de faire remarquer que la façon la plus basique de lire un algorithme en langage naturel est de le lire de haut en bas. Cela afin de mieux introduire les structures de contrôle (on peux souhaiter ommettre ou non de lire du code, ou sauter à la ligne x ou y).
* todo : ajouter cette notion de lecture linéaire haut/bas
* concernant ce dernier point, on peut utiliser comme exemple les 'livres dont vous êtes le héros’, ou même un livre de cuisine qui pour une recette donnée, renverrais à une sous-recette (pour faire une sauce qui entrerait dans la composition d’un plat par exemple).
* des notions présentes dans scratch ne sont pas abordées dans l’introduction, à savoir les notions d’événements et de thread. À voir si cela doit être abordé et comment. Peut-être qu’il n’est pas nécessaire de les aborder dès maintenant, Scratch étant un outil et non une finalité.
* les notions de scope et d’environnement n’ont pas été suffisamment abordées je pense
* il faudra noter des exemples parlant pour différents notions.
* introduire les sorties de boucle
* essayer de développer davantage scratch ? Des étudiants indiquent avoir un peu du mal à d’y retrouver dans les différents « tiroirs » contenant les différents blocs.


## Essayons de revoir le déroulé

On démarre par quelques définitions générales : tout d’abord, qu’est-ce que programmer, ce qui nécéssite de définir algorithme et langage formel ? Avec des exemples. À ce stade on peut faire remarquer qu’un programme est une suite d’instruction lues les unes après les autres. C'estdéjà bien, mais on peut aller plus loin. Que se passe-t-il si l’on veut répéter du code par exemple ? Le recopier maintes fois n’est pas franchement une solution viable. Ou si l’on souhaite n’exécuter du code qui si une condition est remplie ? On introduit ici les structures de contrôle, qui permette cela. Est-ce qu’on passe aux variables (ou aux fonctions) ? Les introduire en indiquant que l’ordinateur n’a pas la capacité de retenir de lui-même des informations d’une ligne à l’autre. Il est nécessaire de lui demander. On utilise pour cela des variables. On *déclare* avoir besoin d’un espace en mémoire. La variable est composée de plusieures choses : un *identificateur*, qui est un nom que l’on lui donne, de préférable permettant de comprendre sa fonction, son usage. Sa valeur, c’est-à-dire la valeur en mémoire auquelle elle correspond. Et enfin ce qu’on appelle une référence, qui est en quelque sorte un point de liaison entre l’identificateur et la valeur en mémoire. Un identificateur ne peut être lié qu’à une seule référence, mais une référence peut être liée à plusieurs identificateur. Elle ne peut par contre être liée qu’à une seule valeur en mémoire.

Bon en fait est-ce qu’on ne devrait pas dire plus tôt qu’un programme peut avoir des entrées, des sorties, un environnement et donc des états ? Montrer un exemple sous forme d’animation ?

Faire un programme genre sous processing pour créer les anim image par image ? En tout cas du libre !
