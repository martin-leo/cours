objectif du jour : transition vers la programmation texte

Nous avons jusqu’ici fait des petits programmes en utilisant des systèmes de blocs. C'estpeu souvent le cas en programmation, où l’on utilise davantage des langages de programmation, des langages formels donc, qui permette à l’humain d’écrire des instructions compréhensibles par une machine.

Mais, que fait la machine lorsque l’on lui soumet un programme ? On va voir une partie de ce processus.

Voyons un programme très basique en processing :

```java
void draw () {
  line( random(100), random(100), random(100), random(100));
}
```

Ce programme est le suivant : une fonction **draw** est définie. Il faut savoir que par défaut, le logiciel Processing appelle en boucle cette fonction.

On y a mis une instruction très simple, qui est un appel à la fonction line, qui se charge de dessiner une ligne entre deux points dont on a défini les coordonnées en x et y. Ici ce sont des nombres aléatoires, on a donc ceci (montrer le sketch).

Alors comment l’ordinateur lit-il ce programme, en gros. Tout d’abord, ce n’est à la base qu’une suite de caractères, qu’il va essayer d’interpréter. Pour cela il va opérer une première phase dite de tokenisation

(en fait faut que je revoie mes notes là c’est quand même pas simple)

Bref, il sépare la suite de caractère en lexèmes, qu’il catégorise en plusieurs catégories :

* identificateurs : des noms définis par le programmeur et l’environnement de développement
* opérateurs : symboles qui dénote des opération sur des données et produisent un résultat
* délimiteurs : symboles de regroupement, de ponctuation, d’assignement/liaison
* littéraux : valeurs classées par type : nombres, booléens, chaînes de caractère
* Commentaires : documentation écrite visant le lecteur humain

https://www.ics.uci.edu/~pattis/ICS-31/lectures/tokens.pdf

Avec l’exemple précédent :

| lexème | catégorie |
| --- | --- |
| void | identificateur |
| draw | identificateur |
| ( | délimiteur |
| ) | délimiteur |
| { | délimiteur |
| line | identificateur |
| ( | délimiteur |
| random | identificateur |
| ( | délimiteur|
| 100 | littéral |
| ) | délimiteur|
| , | délimiteur|
| (etc.) | |
| ) | délimiteur|
| ; | délimiteur|
| } | délimiteur|
| } | délimiteur|

Concernant les opérateurs, voici quelques exemples :

```java
1 + 1 // produit la valeur 2
2 == 2 // produit la valeur « vrai »
```

Un opérateur peut être dit unaire, binaire ou ternaire, selon le nombre de valeur qu’il prend en entrée, par exemple :


```java
// opérateur unaire
a++

// opérateur binaire
1 + 1

// opérateur ternaire
a ? b : c
```

Voyons aussi les commentaires, dont le but est de faciliter la rédaction et la relecture du code, en donnant des indications sur celui-ci. Par exemple en java :

```java
// test si a est vrai. Si oui, renvoie b, sinon renvoie c
a ? b : c

/* il est possible de faire
   un commentaire sur plusieurs lignes
*/
```

En python :

```python
# commentaire sur une ligne

''' commentaire sur
plusieurs lignes
'''
```

On a donc ici un premier niveau d’éléments que l’on utilise dans les programme.

On a vu avec scratch que l’on pouvait utiliser des identificateurs pour nommer des variables.

c’est ce que l’on a vu plus haut, avec draw et line qui sont des identificateurs liées à des variables contenant des procédures/fonctions

Mais ! on ne peux pas utiliser n’importe quel mot comme identificateur.

On peut retenir 2 choses :

- en général les noms commençant par autre chose qu’une lettre ou le signe _ (underscore) sont prohibés
- certains identificateurs sont réservés par le langage, on parle de mots-clés réservés.

Par exemple des mots comme *if*, *else*, *while*, *function*, *number* sont probablement réservés par le langage, tenter de les utiliser comme identificateur renverra une erreur.

Si l’on souhaite nommer un nouvel objet en Français, c’est un peu pareil, on choisira généralement un nom qui n’est pas déjà utilisé dans le langage.


## Quel langage choisir ? Comment l’aborder ?

### Quel langage ?

Une seule réponse : ça dépend.

La programmation sert à résoudre des problèmes en utilisant des ordinateurs et des langages. Mais quel langage ? Il existe plus d’un millier de langage, mais bien moins sont populaires ou encore utilisés. Il en reste cependant un bien grand nombre. Le choix du langage va avoir pour principal facteur le problème que l’on souhaite résoudre. Car certains langages sont davantages adaptés à un type de problème donné. Parfois, il existe déjà des outils très avancé utilisant un langage, et il est donc préférable d’utiliser celui-ci pour en profiter.

Par exemple pour le web côté client (navigateur), vous n’aurez pas vraiment le choix que d’utiliser du JavaScript. Mais si vous souhaitez travailler côté serveur, c’est autre chose, plusieurs langages sont utilisables : PHP, Python, JavaScript, cela dépendra de ce que vous faites et des outils que vous souhaitez utiliser.

Ici mon idée c’est de continuer à vous faire faire de la programmation de manière simple et un peu visuelle, et on va donc continuer avec Processing. Processing est un environnement de programmation en java.

Alors déjà point important :

Java != JavaScript

Java n’a pas grand chose à voir avec JavScript. Si JavaScript s’appelle comme ça, c’est uniquement parce que Java était à la mode à cette époque. Et il serait tout aussi idiot de rapprocher les deux que de confondre Alexandre le grand et Pierre-alexandre.

### Comment l’aborder ?

On va partir sur cette base (qui se retrouve peu ou prou dans la documentation) :

```java
/* La base sous Processing :lorsque l’on lance un programme, l’environnement processing lit le programme, puis appelle la fonction setup un fois, et enfin appelle en boucle la fonction draw */

void setup () {
  noLoop(); // cette fonction désactive l’appel en boucle de draw
}

void draw () {
  // on place ici notre programme principal
}
```

On peut noter ici, tout d’abord que l’on a un programme sous forme de texte, et que celui-ci va être lu de haut en bas.

On peut noter ensuite la manière qui permet, en Java, de déclarer des fonction. Le premier mot indique la valeur renvoyée par la fonction, ici rien (void).

Regardons comment cela se passe en JavaScript et en Python :

**JavaScript**
```javascript
function draw () {
  // des instructions
}
```

**Python**
```python
def draw () :
  # des instructions
```

c’est important de voir qu’il y a une structure semblable malgré des détails un peu différent.


Alors on va aborder processing avec sa documentation : https://processing.org/reference/

On peut voir ici une liste de la plupart des éléments rendus utilisables avec processing. On peut délimiter en gros trois types d’éléments :

- des fonctions (avec des () à la fin)
- des mots-clés (sans () à la fin)
- des types (avec des majuscule).

Essayons de retrouver des choses que l’on connaît, les structures de contrôle : les boucles et conditions.

il en existe principalement de type, les boucle for et while
jetons un oeil à la boucle for
.
