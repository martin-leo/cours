# cours de programmation, cours 6 : portée des variables, tableaux

## Portée des variables

La portée, ou *scope* des variables, c’est l’espace dans lequel une variable est disponible.



```javascript
var a = 5; // déclaration de a

function modifier_a () { // déclaration de la fonction modifier_a
  console.log(a);
}

modifier_a(); // appel de modifier_a
              // imprime -> 5 dans la console
```

```javascript
var a = 5; // déclaration de a

function modifier_a () { // déclaration de la fonction modifier_a
  a = 0; // réassignation de a
}

modifier_a(); // appel de modifier_a

console.log(a); // -> 0

```

Mais que se passe-t’il si l’on fait :

```javascript
var a = 5; // déclaration de a

function modifier_a () { // déclaration de la fonction modifier_a
  var a = 0; // déclaration et assignation de a
  console.log(a); // -> 0
}

modifier_a(); // appel de modifier_a

console.log(a); // -> ?

```
Ici, console.log renverra 5. Ici on déclare une variable a dans un environnement **global**. Puis on déclare une autre variable a dans un environnement **local** à la fonction.

## tableaux et objet

On a vu comment mettre en mémoire une information associée à un identificateur.

On a aussi vu comment mettre en mémoire une procédure (aussi appelée fonction).

Maintenant, que peut-on utiliser si l’on souhaite rassembler plusieurs informations à un seul endroit ?

### tableaux

La plupart des langagesdonnent accès à une structure appelée tableau ou liste.

Il s’agit en effet d’une liste ordonnée de couples **index** -> **valeur**.

Voici par exemple un tableau de longueur 6 :

| index | valeur |
| --- | --- |
| 0 | "Paris" |
| 1 | "Londres" |
| 2 | "Berlin" |
| 3 | "Bruxelles" |
| 4 | "Copenhague" |
| 5 | "Amsterdam" |

On peut remarquer que le tableau est "0-based" : la numération commence à zéro.

### Créer un tableau

Pour déclarer ce tableau en JavaScript :

```javascript
var villes = [ "Paris", "Londres", "Berlin", "Bruxelles", "Copenhague", "Amsterdam"];
```

En Java :

```java
String[] villes = { "Paris", "Londres", "Berlin", "Bruxelles", "Copenhague", "Amsterdam" };
```
### Lire dans un tableau

On utilise l’index d’un élément pour lire dans un tableau :

```javascript
var villes = [ "Paris", "Londres", "Berlin", "Bruxelles", "Copenhague", "Amsterdam"];

var capitale_francaise = villes[0];
console.log(capitale_francaise); // -> Paris
```

```java
String[] villes = { "Paris", "Londres", "Berlin", "Bruxelles", "Copenhague", "Amsterdam" };

String capitale_francaise = villes[0];
println(capitale_francaise); // -> Paris
```

### Écrire dans un tableau

De manière similaire :

```javascript
var villes = [ "Paris", "Londres", "Berlin", "Bruxelles", "Copenhague", "Amsterdam"];

villes[0] = "Lyon";
villes[2] = "Munich";
console.log(villes); // -> [ "Lyon", "Londres", "Munich", "Bruxelles", "Copenhague", "Amsterdam" ]
```

### Quelques petites choses à savoir

Dans certains langages, la taille d’un tableau doit être définie avant de s’en servir, et ne peut changer, c’est-à-dire qu’on ne pourra pas ajouter plus d’éléments au tableau, seulement modifier ceux présents.

d’autres langages permettent d’ajouter ou supprimer des éléments à la demande, et fournissent parfois des outils très pratiques pour cela.

Par exemple en JavaScript :

```javascript
var villes = ["Paris", "Lille", "Nantes"];

villes.push("Lyon"); // ajoute Lyon au tableau
villes.pop(0); // renvoie et retire du tableau l’élément 0, ici Paris
```

#### pile et queue

Ces outils permettent de modéliser  des structures de données simples et pratiques. Imaginez que vous accumulez des éléments dans un tableau par un biais, et que vous souhaitiez ailleurs les traiter :
* Si vous ajoutez des éléments à la fin du tableau et que vous traitez en premier les premiers arrivés, c’est une **queue**,
* si vous ajoutez des éléments à la fin du tableau et que vous traitez les éléments les derniers arrivés, c’est une **pile**.
