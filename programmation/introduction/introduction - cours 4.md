# Cours 4 : introduction au langage

* réponses à d’éventuelles questions sur les projets
* démonstration d’un montage Arduino simple (drapeau réagissant à la lumière)


Ou exercices pratiques avec https://blockly-games.appspot.com/?lang=fr

notamment le labyrinthe
------

##

On a jusqu’ici programmé en utilisant des blocs, mais ça n’est pas ce qu’on utilise le plus souvent. Il existe des logiciels tout à fait aboutis et professionnels qui utilisent des formes similaires de programmation nodale.

Mais le plus souvent, il va falloir écrire. Alors, écrire avec quoi ? qu’est-ce qui est disponible ? On va pour cela essayer de voir comment l’ordinateur « lit » un programme.

Un programme peut-être vu comme une suite de caractères : lettres, espaces, sauts de ligne, nombres, signes de ponctuation. Pour l’ordinateur, il n’y a à ce stade pas de différence entre  les chaînes ```a = b``` et ```dsjkhvldk```. Ce sont juste des chaînes de caractères.

Notez que ce qui suit là n’est pas à savoir par coeur.

------

## Anatomie d’un programme

On peut diviser un programme en différentes éléments :

* les mots-clé (*keywords*)
* les opérateurs (*operators*)
* les délimitateurs (*delimiters*)
* les valeurs littérales (ou *literals* en anglais)
* les identificateurs (*identifiers*)

### Mots-clés, opérateurs, délimitateurs

Les mots-clés, ou mots-clés réservés sont des termes réservés au langage, ils constituent le vocabulaire de base de celui-ci.

### Identificateurs

On a déjà parlé d’identificateurs avec les variables. Ce sont des chaînes de caractères qui permettent


------

## Aborder les langages de programmation

Un langage de programmation est généralement défini par une spécification, qui est un document très complexe et très précis. En règle général vous n’aurez jamais à lire ce document. Des documentations, de plus ou moins bonne qualité, en sont issues, et sont bien plus accessibles. On va donc s’intéresser à ces documentations.

On va jeter un oeil à celle sur la grammaire lexicale du JavaScript sur la documentation du Mozilla Developer Network, ce qui va nous permettre de voir les éléments constitutifs d’un langage : https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Grammaire_lexicale

## Grammaire lexicale

La grammaire formelle, pour faire simple, va être l’ensemble des termes admissibles par un langage. Cela ne recouvre pas sa syntaxe, c’est-à-dire les combinaisons considérées comme correctes de ces mots.

Cette documentation indique différents éléments :

* Caractères de contrôle (**Control characters**) : c’est anecdotique on ne va pas s’y intéresser.
* Blancs (**White space**) : les espaces blancs ont deux fonctions principales : séparer les éléments du code, et augmenter la lisibilité du code, princiapelemtn en permettant de créer des niveau de lecture grâce à des retraits.
* Terminateurs de lignes (**Line terminators**) : Retenez que l’usage du terminateur de ligne « nouvelle ligne » (touche entrée) vous permette de ne pas avoir à écrire tout votre code sur une ligne. Mais qu’il n’est accepté qu’en fin de ligne, si vous souhaitez par exemple créer une valeur chaîne de caractère avec un saut de ligne dedans, il faudra utiliser la séquence d’échappement \n.
* Commentaires (**Comments**) : les commentaires sont des zones de texte libre qui ne sera pas sauf cas très précis, interprété par l’ordinateur : ils servent à commenter le code afin de faciliter sa lecture et sa compréhension. Il est indispensable
* Mots-clés (**Keywords**)
* Littéraux (**Literals**)
* Insertion automatique de points-virgules (**Automatic semicolon insertion**)

Pour moi il manque quelque chose : les délimiteurs (**delimiters**) et les opérateurs (**operators**).

-------

Produire un code simple qui comporte tous les éléments :

* les mots-clé (*keywords*)
* les opérateurs (*operators*)
* les délimitateurs (*delimiters*)
* les valeurs littérales / littéraux (ou *literals* en anglais)
* les identificateurs (*identifiers*)

* blancs
* terminateurs de ligne
* commentaires

```javascript

var suite_initiale = [1, 2];
var taille_suite_finale = 5;
var suite;

function fibonacci (suite, taille_suite_finale) {
  suite.push(suite[suite.length - 2] + suite[suite.length - 1]);
  if (taille_suite_finale > suite.length) {
    return fibonacci (suite, taille_suite_finale);
  } else {
    return suite;
  }
}

suite = fibonacci(suite_initiale, taille_suite_finale);
console.log(suite);
```

Non trop compliqué !

```javascript
// on créé différentes variables
var score_a_atteindre = 6;
var reussite = false;
var tentatives = 0;

// on créé une variable jette_dé et on y met le sous-programme qui suit
function jette_de () {
  var de;
  // on prend un nombre au hasard entre 0 et 1
  de = Math.random();
  // on le multiplie par 6 pour avoir un nombre entre 0 et 6
  de = de * 6;
  // on l’arrondit
  de = Math.round(de);
  // on retourne la valeur
  return de;
}

// tant que la valeur de reussite est false
while (!reussite) { // ici on utilise l’opérateur de négation, car la boucle while s’exécute tant que l’argument est vrai et qu’on souhaite qu’elle s’exécute tant que l’argument est faux.
  var score = jette_de(); // on récupère une valeur entre 0 et 6 grâce au sous-programme jette_de
  tentatives = tentatives + 1;
  if (score == score_a_atteindre) { // si on a tiré le bon chiffre, on met reussite à vrai pour arrêter la boucle
    reussite = true;
    console.log('réussite au bout de ', tentatives, 'tentatives’);
  }
}
```

```javascript
var a;
a = Math.random();

if (a > 0.5) {
  console.log(a, 'est supérieur à 0.5');
} else if (a == 0.5) {
  console.log(a, 'est égal à 0.5');
} else {
  console.log(a, 'est inférieur à 0.5');
}
```

```arduino
Dans un « croquis » Arduino, on fait normalement plusieurs choses. Il s’agit essentiellement de déclarer des variables et des sous-programmes. Deux sous-programme doivent être déclarés car ils seront appelés par la carte : setup et loop.

void setup() {

}

void loop() {

}
```
