# Programmer, une introduction

## introduction

Un langage de programmation est un langage permettant de donner des instructions à un système informatique. C'estcela la programmation : écrire des instruction qu’un système informatique va lire et exécuter. Il en découle qu’un programme informatique est cet ensemble d’instructions que va exécuter l’ordinateur.

Pour apprendre une langue, il faut maîtriser différents aspects :

* Le vocabulaire : les différents mots, verbes, etc. utilisés dans la langue
* La grammaire : la composition correcte des mots en structures. Par exemple l’ensemble de mots « Le parpaing mange la maison » est grammaticalement correct.
* Un dernier aspect est le sens. Vocabulaire et grammaire ne suffisent pas, et la langue comporte un aspect sémantique.

c’est sensiblement la même chose en programmation. Les langages de programmation ont tous un lexique de mots, un syntaxe (leur grammaire) qu’il faut connaître pour pouvoir former des propositions sémantiquement correctes (qui ont du sens).

On peut aussi ajouter que les langues contiennent aussi certains concepts comme la conjugaison, les préfixes, la ponctuation, les figures de style, etc.

c’est la même chose en programmation : il existe de nombreux concepts utiles pour programmer.

## Nommer les choses

Si vous êtes amenés à programmer, il sera important de connaître les termes relatifs à la programmation (et cela aussi bien en français qu’en anglais). Cela notamment pour pouvoir comprendre ce que vous lirez, mettre des mots sur les problèmes que vous rencontrerez, pour pouvoir les expliquer, ou pour pouvoir chercher une solution dans un moteur de recherche.

s’intéresser aux termes précis peut aussi aider à comprendre certains concepts sous-jacents à la programmation.

### Les éléments d’un programme

#### Analyse syntaxique

analyse lexicale : processus de convertir une séquence de caractères (comme un programme informatique ou une page web) en une séquence de jetons (*tokens*, des chaînes de caractère avec un 'sens’ identifié). Un programme procédant à une analyse syntaxique est appelé **analyseur syntaxique** (*lexer*, *tokenizer*, *scanner*)

segmentation PUIS analyse syntaxique

https://en.wikipedia.org/wiki/Lexical_analysis
https://fr.wikipedia.org/wiki/Analyse_lexicale
https://en.wikipedia.org/wiki/Parsing

Un programme n’est finalement qu’une suite de caractères, des lettres, espaces, retours à la ligne (qui sont des caractères même s’ils ne sont souvent pas représentés visuellement), et autres signes. Il est nécessaires pour le système informatisé d’arriver à diviser cette séquence de caractères en différents éléments. On parle d’analyse syntaxique (*lexical analysis*) ou de segmentation (*tokenization*) pour désigner cette action de séparer .


(des **lexèmes**). Un peu lorsque nous isolons les mots cequineselimitepasàregarderoùsontlesespaces.

Cette division se fait grâce à une **analyse lexicale**.

Par exemple, on peut découper le code qui suit en différents **lexèmes**.

```javascript
var jour = true;
```

|lexème|
|---|
|var|
|jour|
|=|
|true|
|;|

Le procéder de démarquer les de
l’étape suivante est de catégoriser les lexèmes, ce que l’on nomme « tokenisation ». Une fois les lexèmes catégorisés (« tokenisés »), on parle de *tokens*, traduits en français **jetons**.

|Token|
|---|
|Lexème|Catégorie de token|
|var|mot-clé|
|jour|identificateur|
|=|opérateur|
|true|valeur littérale|
|;|séparateur|

#### Catégories de jetons

* les mots-clé (*keywords*)
* les identificateurs (*identifiers*)
* les opérateurs (*operators*)
* les délimitateurs (*delimiters*)
* les valeurs littérales (ou *literals* en anglais)

#### Délimitateurs

##### Définition

Les délimitateurs (*delimiters* en anglais) permettent comme leur nom l’indique de délimiter des portions de code. Qui peuvent être des fonctions, commentaires, ou autres. Nous verrons ce qu’ils délimitent par la suite.

##### Exemple

Dans l’exemple ci-dessous, les caractères ```'``` servent à délimiter une chaîne de caractère.

```javascript
'un chaîne de caractères’
```
##### Le délimitateur comme facteur d’erreur

Il est courant d’oublier un délimitateur ou d’en laisser un de trop dans un programme. au mieux, le programme refuse de démarrer et signale une erreur. Au pire, vous risquez des résultats inattendus !

##### Caractères et séquences d’échappement

On a parfois besoin d’utiliser un délimitateur pour autre chose qu’une délimitation, et qu’il faille le préciser explicitement pour éviter toute erreur lors de l’analyse syntaxique.

Prenons cet exemple :

```javascript
var phrase = 'j'aime la programmation !'
' // <-- ce dernier ' est uniquement là
  // afin de neutraliser les bugs engendrés
  // dans l’analyse syntaxique au sein
  // du document source de ce cours !
```

Ici  nous rencontrons un problème. Pas seulement une erreur typographique avec l’utilisation du mauvais signe apostrophe (« ' »  au lieu de « ’ »).

l’apostrophe suivant « j » va en effet être interprétée comme délimitateur. On évite cela en utilisant un caractère ou une séquence d’échappement (on utilisera aussi, dans des langages de balisage comme HTML, des [entités de caractères](https://fr.wikipedia.org/wiki/Liste_des_r%C3%A9f%C3%A9rences_d%27entit%C3%A9s_de_caract%C3%A8res_en_XML_et_HTML) ou character entities).

```javascript
var phrase = 'j\'aime la programmation !'
```

#### mots-clés

##### Définition

Les mots-clés (*keywords* en anglais) sont des séquences de caractères correspondant à une fonctionnalité d’un langage de programmation. Il est très utile de connaître les **mots-clés** d’un langage de programmation, et leur usage, si l’on souhaite écrire un programme ou même simplement en lire un.

##### Exemple
Dans l’exemple qui suit, le terme 'function’ indique ici la présence de ce qu’on appelle une fonction. On observe aussi des délimiteurs '()' et '{}' qui ne contiennent ici rien du tout.

```javascript
function(){}
```

##### Mots réservés

La plupart des langages de programmation définissent des **mots réservés** (*reserved words* en anglais). Ces mots sont dédiés à une usage précis par le langage et ne peuvent être utilisés pour un autre usage, par exemple comme **identificateurs**. (Il peuvent par contre toujours être utilisés dans des chaînes de caractères.) Il est donc utile de les connaître, car les utiliser malgré cela engendrera des erreurs lors de l’éxécution d’un programme.

Généralement, les **mots-clés** sont des **mots réservés** (on parle parfois de *reserved keywords*).

###### Exemple

```javascript
// exemple avec le mot-clé réservé 'var'
// la ligne suivant ne génère pas d’erreur :
var departement = 'var'
// celle-ci génèrera une erreur de syntaxe :
var departement = var
```

#### Valeurs littérales

##### Définition
Une **valeur littérale** (*literal*) est une valeur donnée implicitement dans un code source.

##### Exemple

Dans l’exemple suivant '0' est un littéral, une valeur indiquée directement dans le code source. C'estla même chose pour la séquence de caractères 'Programmer - une introduction’ dont on peut observer qu’elle est délimiter par deux '.

```javascript
var numero_du_cours = 0
var titre_du_cours = 'Programmer - une introduction’
```
#### Identificateurs

Un **identificateur** (*identifier*)

##

Il y a des éléments intéressants ici : https://www.lri.fr/~mbl/ENS/DEUG/cours/3-lexique-syntaxe.html

notamment sur la comparaison avec les langues et langues triplets vocabulaire/grammaire/sens et lexique/syntaxe/sémantique

## sources

* https://www.lri.fr/~mbl/ENS/DEUG/cours/3-lexique-syntaxe.html

## notes de rédaction

Concernant les délimitateurs, montrer du code avec les signes invisibles ! Par exemple pour montrer qu’un commentaires peut être délimité par deux barres inclinées // et un retour chariot

Est-ce que l’espace est un délimitateur ? ; et retour à la ligne ?

Voir aussi token https://en.wikipedia.org/wiki/Lexical_analysis#Token ? et symbol ? (c’est sensé être la même chose ?)


Un document est-il juste une grande suite de *characters* qui peuvent être *parsés* en *tokens* ?
