# Vocabulaire

## Références

* [Lexique bilingue français/anglais pour l’informatique](https://www.info.ucl.ac.be/~pvr/lexicon.html)
* [fr.wiktionary.org - Catégorie : Lexique en français de la programmation](https://fr.wiktionary.org/wiki/Cat%C3%A9gorie:Lexique_en_fran%C3%A7ais_de_la_programmation)

https://www.lri.fr/~mbl/ENS/DEUG/cours/3-lexique-syntaxe.html

## Lexique

|terme (fr)|terme (en)|signification|
|---|---|---|
|**Balise**|Tag||
|**instruction**|statement|Une instruction transforme un état en un autre état.|
|**expression**|expression||
|**déclaration**|||
|**procédure**|||
|**fonction**|function||
|**objet**|object||
|**paramètre**|||
|**argument**|||
|**identificateur/identifieur**|||
|**mot-clé**|||
|**valeur**|||
|**variable**|variable||
|**constante**|constant||
|**affectation**|||
|**déclaration**|||
||scope||
|**lexique**||voir https://www.lri.fr/~mbl/ENS/DEUG/cours/3-lexique-syntaxe.html|
|**syntaxe**||voir https://www.lri.fr/~mbl/ENS/DEUG/cours/3-lexique-syntaxe.html|
|**sémantique**||voir https://www.lri.fr/~mbl/ENS/DEUG/cours/3-lexique-syntaxe.html|
|**arbre**|||
|**parent**|||
|**enfant**|||
|**évaluer**|||
||delimiters||
|**analyse syntaxique**|parse|https://fr.wikipedia.org/wiki/Analyse_syntaxique|
|**analyse lexicale**|lexical analysis ou tokenization||
||lexical grammar||
|**analyseur syntaxique**|parser||
|**séquence d’échappement**|||
|**caractère d’échappement**|||
|**entités de caractère**|character entities| |
|**lexème**|||
|**segmentation**|tokenization||
|**jeton**|token||
|**classe**|class||
|**objet**|object||
|**Abstraction**|||
|**encapsulation**|||
|**encapsulation**|||
|**héritage**|inheritance||
|**polymorphisme**|||
|**cellule**|||
|**fermeture**|||
|**Surcharge**|*overloading*||
|**portée**||d’un identificateur/valeur en mémoire|
