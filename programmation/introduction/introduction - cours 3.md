# Cours 3

## Quelques exercices avec Scratch

### notions

* ―▸algorithme
* programme
* flux
* contrôle de flux
 * ―▸boucles
 * ―▸conditions
* types de données
* ―▸variable
 * identificateur
 * référence
 * valeur en mémoire
 * environnement
* ―▸argument

Ajout de la notion d’événement ?

### Exercices

Le but de ces exercice est de pratiquer la plupart des notions vues précédemment.

Le prétexte sera la création d’un programme dessinant des formes.

#### Exercice 1 : générateur de formes

##### étape 1, variable : compteur de formes

Nous souhaitons créer un nombre fini de formes.

On créé une variable « nombre de formes » pour garder en mémoire le nombre de forme créés.

##### étape 2, fonction, arguments

* Dans scratch, créer une nouvelle fonction. Former un algorithme dessinant une forme.
* Utiliser des arguments pour rendre la forme paramétrique.
* La fonction doit incrémenter (ajouter 1) au compteur (ce qui va nous permettre de savoir combien de formes ont été créés).

##### étape 3, programme parent

Créez un programme général avec une boucle qui appelle votre fonction avec des arguments aléatoires (ou que vous modifiez à chaque fois), tant que la valeur de la variable « nombre de formes » est inférieure à un nombre donné.

#### Exercice 2 : Compteur récursif

Exemple de fonction récursive (un compteur) avec Scratch.

##### Récursivité

###### Définition

> récursif : Qui se définit en s’utilisant soi-même, directement ou indirectement. [^1]

###### Exemples

* Acronymes récursifs : GNU : Gnu is Not Unix

##### Étapes pour une fonction compteur récursive infinie

* On créé une fonction prenant un nombre $nombre en argument. Cette argument va permettre de compter
* La fonction :
  * fait dire au personnage $nombre pendant 1 seconde
  * s’appelle elle-même avec comme argument $nombre+1
* Appeler la fonction dans le programme principal avec comme argument 0

![fonction récursive infinie](images/compteur-récursif-infini.png)

##### Étapes pour une fonction compteur récursive finie

Ici, l’on va introduire une condition qui va permettre de casser la récursion infinie.

* On créé une fonction prenant deux nombres en argument $nombre et $limite. $limite va permettre de former une condition.
* La fonction :
  * fait dire au personnage $nombre pendant 1 seconde
  * si $nombre est inférieur à $limite :
    * s’appelle elle-même avec comme argument $nombre+1 et $limite
* Appeler la fonction dans le programme principal avec comme argument 0 et 10

![fonction récursive finie](images/compteur-récursif-fini.png)


##### Étapes pour une fonction compteur récursive finie (variante)

Dans cette variante, on ne renseigne pas la limite du compteur mais le nombre de fois où l’on va compter, le nombre d’itération, qui diminue à chaque fois.

* On créé une fonction prenant deux nombres en argument $nombre et $itération.
* La fonction :
  * fait dire au personnage $nombre pendant 1 seconde
  * si $itération est supérieur à 0 :
    * s’appelle elle-même avec comme argument $nombre+1 et $limite-1
* Appeler la fonction dans le programme principal avec comme argument 0 et 10.

![fonction récursive finie](images/compteur-récursif-fini-variante.png)

#### Exercice 3 :

[^1]: https://fr.wiktionary.org/wiki/r%C3%A9cursif
