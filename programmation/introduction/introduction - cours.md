# introduction à la programmation (draft)

## La programmation

### qu’est-ce que programmer ?

Programmer, c’est ***exprimer, dans un langage formel à destination des ordinateurs, des algorithmes***.

### qu’est-ce qu’un langage formel ?

#### exemples

* la notation musicale
* grammaire de précision de précision de correction pour les lunettes (todo : exemple)

### qu’est-ce qu’un algorithme ?

#### Étymologie

> « Du nom du mathématicien perse Al-Khwarizmi déformé d’après le grec ancien ἀριθμός, arithmós (« nombre ») »[^1]

n’allez donc pas chercher la moindre information dans ce nom propre.

#### Définitions

> « Un algorithme est une suite finie et non ambiguë d’opérations ou d’instructions permettant de résoudre un problème ou d’obtenir un résultat. »[^1]

> « Un algorithme, c’est tout simplement une façon de décrire dans ses moindres détails comment procéder pour faire quelque chose. Il se trouve que beaucoup d’actions mécaniques, toutes probablement, se prêtent bien à une telle décortication. »[^2]

#### Exemples

* exemple du calcul de l’hypothénuse (X)
* exemple de l’annuaire (CS50)
* exemple du chemin pour aller au travail (CS50)

* Avantage/inconvénients d’un algorithme. toujours des mieux que d’autres mais parmi les mieux c’est selon critère, selon usage.
* input/output
* état de départ, état d’arrivée

### Exercices

#### Exercice du dessin de caractère

##### Matériel

* crayon
* feuille de papier libre ou avec les champs :
  * programmeuse/programmeur
  * ordinateur
  * instructions (liste à puce)
  * champs dessins (carré de papier libre)

##### Énoncé

Chaque étudiant prend une feuille, écrit son nom à côté de la mention 'programmeuse/programmeur'. Il choisit ensuite une lettre de l’alphabet (mais ne l’indique pas sur la feuille), et écrit des instructions, ligne par ligne, permettant de la dessiner. Lorsque tout le monde a terminé, on procède à l’échange des feuilles. Sur la feuille récupéré, l’étudiant écrit son nom à côté de la mention 'ordinateur', puis suit les instructions indiquées sur la feuille et dessine le caractère sur cette même feuille.

> Il convient de choisir une lettre un peu intéressant. Si vous choisissez le l et que vous écrivez « tracez un trait vertical », vous aurez perdu votre temps et votre camarade-ordinateur aussi. Essayez plutôt de trouver une lettre un peu intéressante.

#### Pratique avec Scratch et le module crayon

##### Présentation de Scratch

##### Atelier

Bon du coup avec scratch on peut quand même essayer d’introduire avant les boucles ? On peut introduire « en gros » avant et dans les détails après quand on voit les langages.

## Les langages de programmation

## qu’est-ce qu’un langage de programmation ?

c’est ***un langage formel à destination des ordinateurs***, mais avec la double contrainte d’être compréhensible des humains et des machines.

On peut programmer en langage machine, mais c’est très difficile.

Il existe des milliers de langages de programmation qui partagent toutefois de nombreux concepts. C'estpourquoi il est important de maîtriser les concepts et de dépasser les idiotismes (particularités) des langages : afin de pouvoir passer facilement d’un langage à un autre.

### paradigmes

Il existe de nombreux concepts. Un langage de programmation n’implémente pas tous ces concepts, mais certains de ces concepts. Ces assemblages de concepts sont appelés **paradigmes**. Par exemple, le paradigme le plus populaire est appelé **Programmation Orientée Objet**, mais il n’est qu’un paradigme parmi d’autres. Et ce qui est vrai dans un paradigme n’est pas nécéssairement vrai dans un autre.

### spécifications

Ces langages formelles sont définis par des normes que l’on appelle spécifications. Ce sont des documents techniques qui s’adressent plutôt à ceux qui vont avoir à implémenter le langage (créer le programme qui va permettre à l’ordinateur de le comprendre), vous n’aurez sans doute pas à les consulter.

> Un même nom de langage peut correspondre à plusieurs normes. Par exemple JavaScript est un terme générique qui peut correspondre entre autres à la norme ECMAScript 5 ou 6.

### composantes d’un langage de programmation

### syntaxe, grammaire

### mot-clés, délimitateurs, opérateurs, blancs, sauts de ligne

mots, ponctuation, verbes, noms

## qu’est-ce que la programmation ?

La programmation, c’est bien évidemment programmer. Mais il existe des implications moins évidentes pour bien programmer. La programmation c’est aussi généralement :
* veiller à la qualité de son code (SOLID ?)
* produire un code lisible en faisant attention à la forme, à la documentation. Cela permet de mieux comprendre son code après coup et de mieux se faire comprendre par les autres, car souvent on ne code pas que pour soi, ou seul.
* travailler en équipe : un programme peut contenir des millions de lignes de code, et elles ne sont pas écrites par une seule personnes

## les outils pour programmer

https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Mots_r%C3%A9serv%C3%A9s

Autres :

* faire reformuler

Algo de recherche dans un annuaire (CS50)

Expliquer les choix pédagogiques


[^1]: https://fr.wiktionary.org/wiki/Algorithme
[^2]: https://fr.wikipedia.org/wiki/Algorithme
