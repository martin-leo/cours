# Lexique du cours 1

## programmer

Programmer, c’est ***exprimer, dans un langage formel a destination des ordinateurs, des algorithmes***.

## langage formel

> « Du nom du mathématicien perse Al-Khwarizmi déformé d’après le grec ancien ἀριθμός, arithmós (« nombre ») »[^1]

## algorithme

> « Un algorithme, c’est tout simplement une façon de décrire dans ses moindres détails comment procéder pour faire quelque chose. Il se trouve que beaucoup d’actions mécaniques, toutes probablement, se prêtent bien à une telle décortication. »[^2]

## boucle

En anglais : *loop*.

## condition

En anglais : *condition*.

## valeur

Voir lexique.md

## type

Voir lexique.md

## fonction

Voir lexique.md

## argument

Voir lexique.md

[^1]: https://fr.wiktionary.org/wiki/algorithme
[^2]: https://fr.wikipedia.org/wiki/Algorithme
