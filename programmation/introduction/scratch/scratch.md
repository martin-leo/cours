# Initiation à Scratch et à des Notions essentielles

N.A.: ce document est fortement inspiré d’un fragment du MOOC CS50 de l’université d’Harvard.

## notions abordées

* structure de contrôle
 * boucle
 * condition
* type de donnée
 * booléens
* variable
* fonction
 * arguments

\pagebreak

## Exercices

### Un programme simple

#### Description

Le personnage dit « bonjour ».

#### Programme

![](images/scratch--a-miauler.png)\

\pagebreak

### Un second programme simple

#### Description

Le personnage émet un son.

#### Programme

![](images/scratch--b-bonjour.png)\

\pagebreak

### Condition : if

#### Description

Le personnage émet un son si une condition donnée est vrai, un autre si elle est fausse.

#### Notions

* condition (structure de contrôle)
* booléen (type de donnée)

#### Programme

![](images/scratch--c-condition--1.png)\

![](images/scratch--c-condition--2.png)\

\pagebreak

### Programme s’exécutant en permanence

#### Description

Le personnage émet un son en continu.

#### Notions

* boucle (structure de contrôle)

#### Programme

![](images/scratch--d-boucle--1.png)\

> ça bugge ! Ajoutons un délai.

![](images/scratch--d-boucle--2.png)\

\pagebreak

### le personnage miaule lorsque touché

#### Notions

* boucle (structure de contrôle)
* condition (structure de contrôle)

#### Programme

![](images/scratch--e-evenementielle.png)\

\pagebreak

### le personnage miaule lorsque pas touché et rugit lorsque touché

#### Notions

* boucle (structure de contrôle)
* condition if/else (structure de contrôle)

#### Programme

![](images/scratch--f-if-else.png)\

\pagebreak

### Si x faire y sinon faire z

Le personnage miaule un certain nombre de fois quand on le touche, puis se plaint.

#### Notions

* boucle (structure de contrôle)
* variable

#### Explication complémentaire

Pour réaliser ce programme, on a besoin d’un espace en mémoire permettant de stocker le nombre de fois où le personnage a déjà été touché. On utilise pour cela une *variable* qu’il est nécessaire de déclarer dans données → déclarer une variable.

> On reviendra plus tard sur cette notion de variable.

#### Programme

![](images/scratch--g-variable.png)\

\pagebreak

### Compteur

Le personnage compte.

#### Notions

* boucle (structure de contrôle)
* variable

#### Programme

![](images/scratch--h-compteur.png)\

\pagebreak

### Un petit dessin

Dessiner un carré.

#### Notions

* boucle

#### Programme

##### première version

![](images/scratch--i-dessin--1.png)\

> on observe de nombreuses répétitions : on peut mieux faire !

##### seconde version

![](images/scratch--i-dessin--2.png)\

> ici on met à profit la structure de contrôle qu’est la boucle pour répéter des instructions

\pagebreak

#### Explication complémentaire

On créé ici une fonction (un bloc sous scratch) qui va nous permettre de réutiliser du code.

### Créer une fonction

Création d’une structure réutilisable. On se rend dans « ajouter blocs » et clique sur « créer bloc ».

#### Notions

* boucle
* fonction

#### Programme

![](images/scratch--j-fonction.png)\

#### Résultat

![](images/scratch--j-fonction--resultat.png)\

\pagebreak

### Créer une fonction avec des paramètres

On souhaite que la fonction intègre des paramètres : emplacement, taille.

#### Notions

* paramètre
* fonction

#### Programme

![](images/scratch--k-fonction-parametres.png)\

#### Résultat

![](images/scratch--k-fonction-parametres--resultat.png)\
