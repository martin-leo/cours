# Petit lexique de programmation

Ce document a pour vocation de regrouper un ensemble de termes dont la connaissance est utile voir indispensable pour bien programmer.

## un programme informatique

Un **programme** informatique est une suite d’**instructions** données à l’ordinateur en utilisant un **langage de programmation**.

## langage de programmation

Un **langage de programmation** est, comme son nom l’indique, un langage qui permet de programmer. Ce langage est converti en **code machine** pour être lu par l’ordinateur qui va l’**exécuter**.

Le **code machine**, binaire (0100101, etc.) étant extrêmement peu lisible pour un être humain, on passe par un langage intermédiaire plus compréhensible par ceux-ci pour communiquer avec la machine.

Plus un langage est proche du code machine, plus il est dit **bas niveau**. Plus il est proche du langage naturel, plus il est dit **haut niveau**.

## instruction

En anglais : ***statement***.

(Cela semble plus compliqué qu’il ne le paraissait).

## commentaires

En anglais : *comments*.

Les commentaires sont des zones de texte qui seront ignorés lors de l’exécution du programme.

Ils servent à donner des indications sur le programme et à améliorer sa lisibilité.

Il existe des commentaires monoligne et multilignes.

```JavaScript
// ceci est un commentaire monoligne en JavaScript

/* Ceci est un commentaire
multiligne en JavaScript */
```

```python
# ceci est un commentaire monoligne en Python

''' Ceci est un commentaire
multiligne en Python '''
```

Bien commenter son code est une bonne pratique.

## mots-clés et mots-clés réservés du langage

En anglais *keywords* et *reserved keywords*.

Un langage comporte un ensemble de mots-clés qui vont être compris par le langage. Ils sont souvent réservés, c’est-à-dire qu’on ne peut les utiliser pour autre chose que ce pour quoi ils ont été définis par le langage. Ces mots-clés vont servir à former les programmes.

```javascript
var variable;
if (variable === undefined){
  console.log('variable n\'est pas définie');
}
```
Dans l’exemple précédent, *var*, *if* et *undefined* sont des mots-clés définis par le langage.

## identificateurs

En anglais : **identifier**.

Les identificateurs sont des mots qui renvoient à des valeurs ou à des sous-programmes.

Dans l’exemple précédent, *variable*, *console* et *log* sont des identificateurs définis par la/le programmeuse/programmeur.

## délimitateurs/delimiters

En anglais : *delimiters*.

Les délimitateurs servent à délimiter des parties du code.

Dans l’exemple précédent, *;*, *(* et *)*, *{* et *}* sont des délimitateurs.

## valeur

> En informatique, la valeur d’une donnée (par exemple une variable) correspond à son contenu binaire, à interpréter selon son type.[^valeur-wikipedia]

## types de données

En anglais : *types* ou *data types*.

> En programmation informatique, un type de donnée, ou simplement type, définit les valeurs que peut prendre une donnée, ainsi que les opérateurs qui peuvent lui être appliqués.[^type-wikipedia]

## opérateurs

En anglais : *operators*.

> en programmation informatique, un opérateur est une fonction dont l’identificateur s’écrit avec des caractères non autorisés pour les identificateurs fonctions ordinaires.[^operateur-wikipedia]

Il existe des opérateurs *unaires*, *binaires* et *ternaire*, prenant respectivement un, deux ou trois arguments.

## variables et constantes

En anglais : *variables* et *constants*

## expressions

En anglais : *expressions*.

> An expression in a programming language is a combination of one or more explicit values, constants, variables, operators, and functions that the programming language interprets (according to its particular rules of precedence and of association) and computes to produce ("to return", in a stateful environment) another value. This process, as for mathematical expressions, is called evaluation.[^expression-en]

> Une expression, dans un langage de programmation, est une combinaison d’une ou plusieurs valeurs explicites, constantes, variables, opérateurs, et fonctions que le langage de programmation interprète (en accord avec ces règles particulières de précédence et d’association) et effectue un calcul pour produire (« retourner », dans un environnement à états) une autre valeur. Ce processus, comme pour les expressions mathématiques, est appelée évaluation.[^expression-en]

> Dans les langages de programmation, une expression est un élément de syntaxe qui combine un ensemble de lexèmes retournant une valeur.

> c’est une combinaison de littéraux, de variables, d’opérateurs, et de fonctions qui est évaluée (ou calculée) en suivant les règles de priorité et d’associativité du langage de programmation pour produire (ou retourner) une nouvelle valeur.[^expression-fr]

### Exemples

> ma_fonction() // fonction renvoyant une valeur

> ma_fonction() > 0

> $valeur

> $valeur + 1

> 1 + 1

> i++

## évaluer

En anglais : *evaluate*.

> Analyser une expression afin de produire la ou les valeurs correspondantes (le résultat).

> (...)

> Le résultat est une « évaluation ».[^evaluer]

## instructions

En anglais : *statements*.

## structure de contrôle

> En programmation informatique, une structure de contrôle est une instruction particulière d’un langage de programmation impératif pouvant dévier le flot de contrôle du programme la contenant lorsqu’elle est exécutée.[^structure-de-controle]

## fonctions

En anglais : *function*.

Une fonction (parfois appelée *procédure*) est un ensemble d’instruction qui peut avoir des *arguments* (aussi appelés *paramètres*) que l’on peut lui *passer*, et qui peut *retourner* une valeur.

Une fonction peut être placée dans une *variable* et se voir à se titre attribuer un *identificateur* permettant de l’*appeler*.

programme -> sous-programme -> sous-sous-programme

## retourner

En anglais : *return*.

Se dit d’une fonction qui, lors de son évaluation, produit une valeur. Elle retourne une valeur.

## exception

En anglais : *exception*.

## attention

JAVA != JAVASCRIPT

Java et JavaScript sont deux langages qui n’ont pas grand chose de commun. JavaScript à été appelé JavaScript car Java était populaire à ses débuts. C'estpur Marketing et comme trop souvent avec les produits du marketing c’est bon pour la poubelle.

[^expression-en]: https://en.wikipedia.org/wiki/Expression_%28computer_science%29

[^expression-fr]: https://fr.wikipedia.org/wiki/Expression_%28informatique%29

[^type-wikipedia]: https://fr.wikipedia.org/wiki/Type_%28informatique%29

[^type-jargonf]: http://jargonf.org/wiki/type

[^valeur-wikipedia]: https://fr.wikipedia.org/wiki/Valeur

[^evaluer]: http://jargonf.org/wiki/%C3%A9valuer

[^operateur-wikipedia]: https://fr.wikipedia.org/wiki/Op%C3%A9rateur

[^structure-de-controle]: https://fr.wikipedia.org/wiki/Structure_de_contr%C3%B4le
