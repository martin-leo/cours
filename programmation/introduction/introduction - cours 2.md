# Notions abordées la semaine précédente

## algorithme

> « Un algorithme, c’est tout simplement une façon de décrire dans ses moindres détails comment procéder pour faire quelque chose. Il se trouve que beaucoup d’actions mécaniques, toutes probablement, se prêtent bien à une telle décortication. »[^1]

## programme

Expression d’un algorithme dans un langage qui peut être converti en langage machine.

### Exemples

Quelques exemples de projet où l’ordinateur et sa programmation ont joué un rôle significatif.

#### Éditions Jean Boîte — Google Volume 1

Cet ouvrage est un dictionnaire. Pour chaque mot du dictionnaire, la première image renvoyée par Google Image a été utilisée en lieu et place d’une définition. l’ordinateur a pu ici collecter automatiquement les très nombreuses images, puis les mettre en page.

![couverture](images/google-vol-1--0-couverture.jpeg)
![intérieur](images/google-vol-1--1-interieur.jpeg)

[La page du projet](http://www.jean-boite.fr/box/google-volume-1)

#### Basil.js

Développé à Bâle dans différentes écoles d’art et de design, basil.js permet de simplifier la programmation dans le logiciel Adobe Indesign et de créer des mises en pages et/ou manipuler des données programmatiquement.

Un exemple de projet : #onesecond est un projet éditorial regroupant tous les tweet émis mondialement à une seconde donnée.

![vue globale](images/twitter-1-second--0-global-view.jpg)
![détail](images/twitter-1-second--1-detail.jpg)
![détail](images/twitter-1-second--2-detail.jpg)

[Voir de nombreux projets réalisés grâce à basil.js](http://basiljs.ch/gallery/)

#### Rafaël Rozendaal

Rafaël Rozendaal développe depuis des années des dispositifs plastiques sous forme de sites internets, qui sont de petits programmes très visuels.

[Voir les projets sur le site de Rafaël Rozendaal](http://www.newrafael.com/websites/)

#### Albertine Meunier - Angelino

Le dispositif, connecté, détecte les tweets comportant le mot ange et les signale en s’animant.

![Albertine Meunier - Angelino](images/albertine-meunier--angelino.jpg)

[Voir sur le site d’Albertine Meunier](https://albertinemeunier.net/angelino/)

#### Smiirl

La société Smiirl vend des ***« connected social-counters »***. Ces dispositifs affichent en temps réel les likes, tweets, etc. associés à un compte sur un réseau social. On peut voir cela comme une tangibilisation de l’espace non-tangible des réseaux sociaux.

![Smiirl - compteur connecté](images/smiirl--facebook.jpg)

[Voir en ligne](https://smiirl.com)

### conclusion

Tous ces projets utilisent des ordinateurs. l’ordinateur est très pratique pour travailler avec des données, comme pour le livre Google Vol. 1. Ou pour créer de l’interactivité. Voire les deux.

Mais pour mener à bien ces projets, il faut expliquer à l’ordinateur ce qu’il doit faire. C'estcela la programmation : écrire des instructions à destination de l’ordinateur, que celui-ci va bêtement lire et appliquer.

Un autre usage en design peut être celui suivant : vous avez un projet au sein duquel vous prévoyez des interactions. Vous faites alors des suppositions sur ce qui va se passer. Pour vérifier si ces suppositions sont exactes, pour itérer, il vous faut un prototype. La programmation est un des outils pour créer ce prototype.

## flux et contrôle de flux

On a vu lors de l’exercice du dessin de lettre qu’on exécutait le code de haut en bas. C'estce qu’on peut appeler le **flux** : l’ordinateur exécute les instruction l’une après l’autre.

On a aussi vu des exceptions à cela : des **structures de contrôles** (on pourrait dire **structures de contrôle de flux**) :
* les **boucles** : permettent de réexécuter plusieurs fois un ensemble d’instructions,
* les **conditions** : permettent de ??? l’exécution d’un bloc de code à une condition.

## types de donnée

On a (trop) rapidement vu les types de données. Voyons quelques types :

* nombres (il peut exister plusieurs types de nombre, comme par exemple un type entier et un type à virgule)
* booléens (valeur égale soit à vrai soit à faux)
* chaînes de caractères
* et bien d’autres

todo : Introduire le fait qu’une fonction peut être un type de donnée ? types primitifs/composites (voir les termes)

Pour l’ordinateur, ce n’est pas la même chose de travailler avec le nombre zéro ou le caractère '0'. Il faut en être conscient car cela à des implications qui seront traitées plus tard.

## variable

On a vu qu’une variable permettait de conserver une information.

On peut donner un calcul à faire à un ordinateur, par exemple trois fois quatre. Mais si l’on ne lui demande pas explicitement de se souvenir du résultat, il ne s’en souviendra pas l’instruction d’après. donc si on en a besoin pour plus tard, il faut explicitement lui demander de réserver un emplacement de sa mémoire, et d’y stocker ladite information.

Le nom variable est un nom utilisé pour désigner de nombreuses choses, aussi il est préférable de passer en revue ce que le terme contient.

Le terme « variable » comprend donc ces notions

- identificateur (son nom)
- référence (lien vers une valeur en mémoire)
- valeur en mémoire (là où est stocké l’information)

Le nom **variable** vient du fait que la valeur peut changer. Une variable dont la valeur ne peut être changée est appelée *constante*.

### schéma

```
identificateur―――▸référence―――▸valeur
```
exemple :

```javascript
var nombre = 5;
```

Correspond à :

```
nombre―――▸référence―――▸5
```

> Dans les langage dit typés, la variable à aussi un type (on ne pourra pas stocker du texte dans une variable de type nombre par exemple)

### manipulation par valeur ou référence

Une chose à savoir, c’est qu’un langage donné peut manipuler certaines données par valeur ou référence.

Prenons un exemple en JavaScript. Ne vous attardez pas sur la notion d’objet que l’on verra plus tard.

#### Copie par valeur

c’est le fonctionnement le plus évident.

```javascript
var original = 5;
var copie = original;
original = 0;
console.log(copie);
// > 5
// La copie est toujours à la valeur donnée lors de la copie.
```

On pourrait schématiser sous la forme :

```
original―――▸référence―――▸ valeur
   copie―――▸référence―――▸ valeur
```

#### Copie par référence

Un fonctionnement qui peut susciter l’incompréhension si on y est pas sensibilisé.

```javascript
// on créé un objet avec la propriété valeur égal à 5
var objet_original = { valeur : 5 }
// on fait une copie de l’objet
var copie = objet_original;
// on modifie l’objet original
objet_original.valeur = 0;
// on regarde la propriété valeur de la copie
console.log(copie.valeur);
// > 0
// elle est égale à zéro
// on change notre copie
copie.valeur = 2;
// on regarde la valeur de notre original
console.log(objet_original.valeur);
// il est égal à deux
```

Ici on pourrait s’attendre à ce que la propriété valeur de la copie soit toujours égal à 5. Mais on n’a pas en réalité créé une copie indépendante de notre objet originale. On a en fait copié sa référence qui renvoie toujours à la valeur originale.

On pourrait schématiser sous la forme :

```
objet_original―――▸référence―――┐
                              ├―――▸ valeur
         copie―――▸référence―――┘
```

#### Conclusion

Bien faire attention au comportement du langage lors de la manipulation des valeurs afin de ne pas avoir de mauvaises surprises !

## Fonction et arguments

### Fonctions

Nous avons vu les fonctions, qui sont des blocs de code indépendants, et que l’on peut placer dans une variable possédant un identificateur afin de pouvoir les réutiliser.

> On peut aussi voir les fonction comme une manière de contrôler le flux.

> On parle, lors de l’usage d’une fonction, d’**appel** de fonction ou parfois d’**invocation**.

### Arguments

Il est possible de passer à ces fonctions des valeurs, appelées arguments.

```javascript
function additionner (x, y) {
  /* Cette fonction additionne x et y et renvoie le résultat */
  return x + y;
}

console.log(addition(1, 1));
```
> Les arguments permettent de créer des fonctions très polyvalentes

## Exercice

### Exécuter un programme

#### Matériel

* une feuille avec une matrice 10 par 10 et la figuration d’emplacement mémoire
* des instructions

#### Programme

* déclarer la variable &lt;dessiner>
* déclarer la variable &lt;n>
* déclarer la variable &lt;curseur_x>
* déclarer la variable &lt;curseur_y>
* assigner à la variable &lt;dessiner> la valeur fonction en annexe I
* assigner à la variable &lt;curseur_x> la valeur 1
* assigner à la variable &lt;curseur_y> la valeur 1
* assigner à la variable &lt;n> la valeur 5
* lever le stylo
* exécuter la fonction &lt;dessiner> avec comme paramètre la valeur de &lt;n>
* assigner à la variable &lt;curseur_x> la valeur 1
* assigner à la variable &lt;curseur_y> la valeur 3
* assigner à la variable &lt;n> la valeur 3
* exécuter la fonction &lt;dessiner> avec comme paramètre la valeur de &lt;n>

#### Annexe I : fonction

* arguments : répétition
* déclarer une variable répétition et y assigner la valeur répétition
* placer le curseur en &lt;curseur_x>, &lt;curseur_y>
* baisser le stylo
* tant que la valeur lié à répétition est >= 0 :
  * ajouter 1 à &lt;curseur_x>
  * déplacer le stylo en &lt;curseur_x>, &lt;curseur_y>
  * ajouter 1 à &lt;curseur_y>
  * déplacer le stylo en &lt;curseur_x>, &lt;curseur_y>
  * soustraire 1 à repetition
* lever le stylo
* supprimer la variable répétition (libérer l’espace en mémoire)

## Draft notes

Indiquer que programme = pile d’instruction et environnement

[^1]: https://fr.wikipedia.org/wiki/Algorithme
