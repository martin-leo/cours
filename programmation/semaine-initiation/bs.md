todo : imprimer trombi chaque groupe

Introduire avec des questions, histoire de sonder un peu le terrain avant de les avoir au prochain semestre, donc répondre à quelques questions sur une feuille, pas besoin de mettre leur nom, et il n’y a pas de « bonne » réponse, ici ce qui m’intéresse c’est de sonder ce qu’évoque pour vous la programmation, ce que vous penser dont il s’agit. Je ne vous demande pas une définition encyclopédique, mais d’essayer, en une à trois lignes, de me dire :

* si vous avez déjà fait quelque chose qui s’apparente à de la programmation et quoi
* qu’est-ce que c’est pour vous la programmation/programmer ?
* pour vous, à quoi ça sert la programmation en général
* et en art/design/architecture ?
* à quoi ça pourrait vous servir durant votre scolarité ?
* y'a-t-il des enjeux à savoir programmer lorsque l’on est artiste, designer, architecte ? Lesquels ?

Je vous laisse quinze minutes.

On va commencer par répondre à la première question. C'estquoi la programmation, où programmer. Mais avant de vous donner une définition déjà faite, j'aimerais bien qu’on discute un peu ça ensemble, et du coup que vous me disiez ce que vous avez écrit.

(échange sur les réponses)

on donne la première partie de la définition du cours 1 : **programmer, c’est exprimer des algorithmes**

à ce stade vous devriez logiquement avoir un peu l’impression de vous être fait avoir parce que cette définition ne résoud rien du tout. Il faut maintenant répondre à la question : c’est quoi un algorithme.

Alors première chose, si on cherche du côté de l’étymologie, on ne va rien trouver :

> « Du nom du mathématicien perse Al-Khwarizmi déformé d’après le grec ancien ἀριθμός, arithmós (« nombre ») »

#### Définitions

> « Un algorithme est une suite finie et non ambiguë d’opérations ou d’instructions permettant de résoudre un problème ou d’obtenir un résultat. »[^1]

> « Un algorithme, c’est tout simplement une façon de décrire dans ses moindres détails comment procéder pour faire quelque chose. Il se trouve que beaucoup d’actions mécaniques, toutes probablement, se prêtent bien à une telle décortication. »[^2]


Alors je vais maintenant vous donner un premier exemple d’algorithme que vous aller exécuter. Dans cet exercice vous allez écrire un algorithme permettant de dessiner une lettre de l’alphabet. Prenez-en une pas trop simple, si vous prenez le o et que vous écrivez « dessinez un rond », certes c’est très malin mais ça n’a aucun intérêt. Point important : vous n’écrivez pas la lettre ni ne la mentionnez sur la feuille.

* se munir d’une feuille et d’un crayon
* écrire sur la feuille son nom précédé du terme « programmeuse/programmeur »
* penser à une lettre de l’alphabet pas trop simple, mais ne pas l’écrire ou la mentionner sur la feuille
* écrivez ligne après ligne, instruction après instruction, comment dessiner la lettre.

Voilà, vous venez d’écrire votre premier programme !
Une fois que c’est fait, on va exécutez le programme qui suit :

* passer sa feuille à son voisin de droite
* passez les deux premières feuilles que votre voisin de gauche vous passe à votre voisine de droite, dans l’ordre où vous les recevez
* gardez la troisième feuille que vous passe votre voisin de gauche
* écrivez : ordinateur suivi de votre nom
* exécutez le programme écrit sur la feuille dans un espace libre de ladite feuille.
* une fois le programme de la feuille terminé, passez la feuille à la programmeuse/au programmeur

À ce stade, à peu près la moitié des programmeurs doivent se dire qu’il aurait peut=être dus être un peu moins ambigüs dans leur programme !

On va donc jetez un oeil à la définition complềte de la programmation que je vais vous laisser :

Programmer, c’est ***exprimer, dans un langage formel à destination des ordinateurs, des algorithmes***.

Alors on a encore un dernier point à voir, c’est cette question du langage formel.

### qu’est-ce qu’un langage formel ?

Je ne saurais pas vous décrire très précisemment et en détail ce qu’est un langage formel, mais on va dire que c’est un ensemble de mots, une syntaxe et une grammaire qui vont nous permettre d’exprimer quelque chose.

#### exemples

* la notation musicale
* grammaire de précision de précision de correction pour les lunettes (todo : exemple)

Un des intérêts de ces langages formel est justement de supprimer l’ambiguïté, car on veut ici décrire des choses très précisemment.

## deuxième question : à quoi ça sert ?

La encore, on va revenir à ce que vous avez pu écrire. De manière générale, à quoi ça sert ?

Demander aux étudiants ce qu’ils ont écrit.

Aujourd’hui, la programmation est partout. Prenons la chaise où vous êtes assis. la programmation a été présente à quasiment tous les stades de sa conception à son arrivée ici, que ce soit au travers des logiciels de conception ou d’administration, commande, contrôle des machines de production, télécommunication. Aujourd’hui, je pense pouvoir dire sans risque que la majorité de ce qui est conçu l’est au moins partiellement sur ordinateur, de la paire de chaussette au réacteur nucléaire.

# Et en art/design/architecture ?

Quelques projets en art/design

### Exemples

Quelques exemples de projet où l’ordinateur et sa programmation ont joué un rôle significatif.

#### Éditions Jean Boîte — Google Volume 1

Cet ouvrage est un dictionnaire. Pour chaque mot du dictionnaire, la première image renvoyée par Google Image a été utilisée en lieu et place d’une définition. l’ordinateur a pu ici collecter automatiquement les très nombreuses images, puis les mettre en page.

![couverture](images/google-vol-1--0-couverture.jpeg)
![intérieur](images/google-vol-1--1-interieur.jpeg)

[La page du projet](http://www.jean-boite.fr/box/google-volume-1)

#### Basil.js

Développé à Bâle dans différentes écoles d’art et de design, basil.js permet de simplifier la programmation dans le logiciel Adobe Indesign et de créer des mises en pages et/ou manipuler des données programmatiquement.

Un exemple de projet : #onesecond est un projet éditorial regroupant tous les tweet émis mondialement à une seconde donnée.

![vue globale](images/twitter-1-second--0-global-view.jpg)
![détail](images/twitter-1-second--1-detail.jpg)
![détail](images/twitter-1-second--2-detail.jpg)

[Voir de nombreux projets réalisés grâce à basil.js](http://basiljs.ch/gallery/)

#### Rafaël Rozendaal

Rafaël Rozendaal développe depuis des années des dispositifs plastiques sous forme de sites internets, qui sont de petits programmes très visuels.

[Voir les projets sur le site de Rafaël Rozendaal](http://www.newrafael.com/websites/)

#### Albertine Meunier - Angelino

Le dispositif, connecté, détecte les tweets comportant le mot ange et les signale en s’animant.

![Albertine Meunier - Angelino](images/albertine-meunier--angelino.jpg)

[Voir sur le site d’Albertine Meunier](https://albertinemeunier.net/angelino/)

#### Smiirl

La société Smiirl vend des ***« connected social-counters »***. Ces dispositifs affichent en temps réel les likes, tweets, etc. associés à un compte sur un réseau social. On peut voir cela comme une tangibilisation de l’espace non-tangible des réseaux sociaux.

![Smiirl - compteur connecté](images/smiirl--facebook.jpg)

[Voir en ligne](https://smiirl.com)

-> rhino grasshopper

## enjeux

Texte de Kévin Donnot

* empreinte de l’outil/uniformisation
  * travailler contre cette uniformisation
  * faire des choix
  * créer ses propres outils avec leur propre empreinte  (exemple TexTuring ?)
* prototyper
  * tester ses suppositions pendant le développement du projet et pas après sa finalisation :
    * cycles de design itératif
    * en créant des prototypes (on peut aussi créer des prototypes sans programmation, mais ça peut aider)

## hour of code 1

http://blockly-games.appspot.com/

## Arduino

### introduction

Arduino est un projet développé en Italie dans le but de rendre le prototypage plus facile, rapide et abordable.

Il s’agit de cartes programmables qui peuvent gérer des entrée et sorties.

### entrées, sorties

On peut donc avoir de l’information qui rentre et de l’information qui sort.

### capteurs actionneurs

Les capteurs produisent de l’information, les actionneurs la transforme.

#### exemples de capteurs

* petite revue des capteurs de l’atelier
* capteurs de luminosité
* capteurs de présence
* capteurs de température
* caméra vidéo
* etc.

#### exemple d’actionneurs

* petite revue des actionneurs de l’atelier
* moteur continu
* servomoteurs
* etc.

### exemple

exemple du montage lumineux / servo

Ici on a un montage très simple : en entrée en capte la luminosité, en sortie on actionne un servomoteur

Montrer le code

## hour of code 2

https://studio.code.org/s/artist/stage/1/puzzle/1








## Questions

* À quoi sert la programmation ?
  * les ordinateurs, les données
  * comment la programmation à changé le monde
* À quoi sert la programmation en art et design ?
* Pourquoi apprendre la programmation en art et design ?
  * autonomie
    * vis à vis des personnes
    * vis à vis des outils existants (la marque de l’outil, notamment sur le web)
    * capacité d’exploration
    * conscience des possibilités et limites des outils
      * on évite de partir dans n’importe quoi ou de s’aperçevoir au dernier moment que ça ne marche pas, que ce n’est pas possible
* qu’est-ce qu’on programme en art et design ?
  * exemple de projets
    * dispositifs
    * logiciels
    * outils
    * sites internet
* Où et comment apprendre à programmer ?
  * l’anglais c’est important
  * les écueils
    * la pédagogie du tutoriel (et leur intérêt s’il faut en trouver un)
    * les nombreux langages/environnements (c’est aussi pour ça qu’il faut voir les principes généraux et pas les spécificités)
  * les trucs cools
    * Mooc MIT, Harvard
    * formations vidéos
    * euh... les gros bouquins o'reilly ?


* Pratique
 * hour of code
 * page web ?
 * une restitution web ?
 * montrer du matos tangible  * arduino avec l’exemple capteur luminosité/servo

* pour la suite


## heure par heure

### H1

  discussion sur la programmation

### H2

  démonstration arduino et présentation logiciels

### H3

  hour of code ?

### La pause

### H4



### H5



### H6


## vrac

05 mn : installation
10 mn : discussion - sonder les attentes, les préconceptions, qui a fait quoi par le passé
10 mn : un petit historique de l’ordinateur et de la programmation
15 mn : discussion - qu’est-ce que la programmation à changé ? Ou que n’a-t-elle pas changé ?
30 mn : montrer des projets où la programmation à un rôle

première heure large là

10 mn : qu’est-ce que la programmation ? qu’est-ce qu’on va faire, qu’est-ce qu’on ne va pas faire ? parallèle avec la peinture, ou le dessin, on ne manipule pas des molécule de carbone pour dessiner, c’est trop bas niveau, on utilise des outils qui sont basés là-dessus, mais quand on dessine on s’ennuie pas à regarder les molécules, ce n’est pas ce qui nous intéresse. Nous on va essayer de voir qu’on a un environnement qui nous donne des possibilités et s’y intéresser. Ça n’est pas inintéressant d’aller voir les molécules, mais ça n’est pas ici ce que l’on souhaite faire.

bon endroit pour embrayer avec hour of code ?

puis les outils et logiciels courants :-> qui permettent de faire du tangible, de traiter des données,
