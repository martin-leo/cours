# Présentation de l’atelier programmation à l’ESAD

## sommaire

* quelques questions à se poser
  * qu’est-ce que la programmation / programmer ?
    * discussion sur la base des réponses des étudiants
    * quelques définitions
      * programmation
      * algorithme
        * un petit exercice pratique
        * langage formel
  * à quoi ça sert (en général) ?
    * discussion sur la base des réponses des étudiants
    * une réponse
  * à quoi ça sert en art/design/architecture
    * discussion sur la base des réponses des étudiants
    * quelques exemples de projets
      * Éditions Jean Boîte — Google Volume 1
      * Basil.js
      * Rafaël Rozendaal
      * Albertine Meunier - Angelino
      * smiirl
      * TexTuring
  * à quoi ça pourrait vous servir dans votre scolarité ?
    * discussion sur la base des réponses des étudiants
  * quels enjeux pour la programmation
    * lecture du texte code = design
    * enjeux :
      * empreinte de l’outil et uniformisation de la production
        * utiliser un langage de programmation pour utiliser différemment le logiciel (basil.js)
        * créer ses propres outils avec leur empreinte propres (TexTuring)
      * Manipuler de l’information et de la complexité
        * l’ordinateur peut faire facilement des choses pour nous difficile : se répeter, traiter beaucoup de données (Google Vol. 1)
      * tester ses suppositions et explorer les possibles avec des prototypes
        * design itératif
  * Concrêtement, on va utiliser quoi dans l’atelier programmation
    * Les logiciels
      * la base : l’éditeur de texte
        * coloration syntaxique
        * numérotation des lignes
        * auto-complétion
      * les environnements de programmation
        * processing
        * arduino
        * pure-data
        * Suite Adobe, logiciels 3D, web
    * les langages de programmation
    * le matériel
     * Arduino
     * Raspberry Pi
     * autres
  * Comment et où se former
  * Conclusion

## Quelques questions à se poser

Sur une feuille de papier, en une quinzaine de minutes, répondre aux question suivantes en une à quelques lignes chacune :

* si vous avez déjà fait quelque chose qui s’apparente à de la programmation et quoi
* qu’est-ce que c’est pour vous la programmation/programmer ?
* pour vous, à quoi ça sert la programmation en général
* et en art/design/architecture ?
* à quoi ça pourrait vous servir durant vos études ?
* y'a-t-il des enjeux à savoir programmer lorsque l’on est artiste, designer, architecte ? Lesquels ?

### qu’est-ce que la programmation / programmer ?

#### discussion sur la base des réponses des étudiants

#### quelques définitions

##### programmation (partie 1)

> programmer, c’est exprimer des algorithmes

##### algorithme

###### étymologie

> « Du nom du mathématicien perse Al-Khwarizmi déformé d’après le grec ancien ἀριθμός, arithmós (« nombre ») »

###### définitions

> « Un algorithme est une suite finie et non ambiguë d’opérations ou d’instructions permettant de résoudre un problème ou d’obtenir un résultat. »

> « Un algorithme, c’est tout simplement une façon de décrire dans ses moindres détails comment procéder pour faire quelque chose. Il se trouve que beaucoup d’actions mécaniques, toutes probablement, se prêtent bien à une telle décortication. »

###### un petit exercice pratique

Dans cet exercice vous allez écrire un algorithme permettant de dessiner une lettre de l’alphabet. Prenez-en une pas trop simple. Si vous prenez le o et que vous écrivez « dessinez un rond », c’est certes très malin mais ça n’a aucun intérêt.

> Point important : n’écrivez pas la lettre ni ne la mentionnez sur la feuille !

**algorithme de l’exercice**

* se munir d’une feuille et d’un crayon
* écrire sur la feuille son nom précédé du terme « programmeuse/programmeur »
* penser à une lettre de l’alphabet pas trop simple, mais ne pas l’écrire ou la mentionner sur la feuille
* écrivez ligne après ligne, instruction après instruction, comment dessiner la lettre.

* passer sa feuille à son voisin de droite
* passez les deux premières feuilles que votre voisin de gauche vous passe à votre voisine de droite, dans l’ordre où vous les recevez
* gardez la troisième feuille que vous passe votre voisin de gauche
* écrivez : ordinateur suivi de votre nom
* exécutez le programme écrit sur la feuille dans un espace libre de ladite feuille.
* une fois le programme de la feuille terminé, passez la feuille à la programmeuse/au programmeur

###### langage formel

Je ne saurais pas vous décrire très précisemment et en détail ce qu’est un langage formel, mais on va dire que c’est un ensemble de mots, une syntaxe et une grammaire qui vont nous permettre d’exprimer quelque chose.

**exemples**

* la notation musicale
* grammaire de précision de précision de correction pour les lunettes (todo : exemple)

Un des intérêts de ces langages formel est justement de supprimer l’ambiguïté, car on veut ici décrire des choses très précisemment.

##### programmation (partie 2)

> Programmer, c’est ***exprimer, dans un langage formel à destination des ordinateurs, des algorithmes***.

### à quoi ça sert (en général) ?

#### discussion sur la base des réponses des étudiants

#### une réponse

Aujourd’hui, la programmation est partout. Prenons la chaise où vous êtes assis. la programmation a été présente à quasiment tous les stades de sa conception à son arrivée ici, que ce soit au travers des logiciels de conception ou d’administration, commande, contrôle des machines de production, télécommunication. Aujourd’hui, je pense pouvoir dire sans risque que la majorité de ce qui est conçu l’est au moins partiellement sur ordinateur, de la paire de chaussette au réacteur nucléaire.

### à quoi ça sert en art/design/architecture

#### discussion sur la base des réponses des étudiants

#### quelques exemples de projets

##### Éditions Jean Boîte — Google Volume 1

Cet ouvrage est un dictionnaire. Pour chaque mot du dictionnaire, la première image renvoyée par Google Image a été utilisée en lieu et place d’une définition. l’ordinateur a pu ici collecter automatiquement les très nombreuses images, puis les mettre en page.

![couverture](images/google-vol-1--0-couverture.jpeg)
![intérieur](images/google-vol-1--1-interieur.jpeg)

[La page du projet](http://www.jean-boite.fr/box/google-volume-1)

##### Basil.js

Développé à Bâle dans différentes écoles d’art et de design, basil.js permet de simplifier la programmation dans le logiciel Adobe Indesign et de créer des mises en pages et/ou manipuler des données programmatiquement.

Un exemple de projet : #onesecond est un projet éditorial regroupant tous les tweet émis mondialement à une seconde donnée.

![vue globale](images/twitter-1-second--0-global-view.jpg)
![détail](images/twitter-1-second--1-detail.jpg)
![détail](images/twitter-1-second--2-detail.jpg)

[Voir de nombreux projets réalisés grâce à basil.js](http://basiljs.ch/gallery/)

##### Rafaël Rozendaal

Rafaël Rozendaal développe depuis des années des dispositifs plastiques sous forme de sites internets, qui sont de petits programmes très visuels.

[Voir les projets sur le site de Rafaël Rozendaal](http://www.newrafael.com/websites/)

##### Albertine Meunier - Angelino

Le dispositif, connecté, détecte les tweets comportant le mot ange et les signale en s’animant.

![Albertine Meunier - Angelino](images/albertine-meunier--angelino.jpg)

[Voir sur le site d’Albertine Meunier](https://albertinemeunier.net/angelino/)

##### smiirl

La société Smiirl vend des ***« connected social-counters »***. Ces dispositifs affichent en temps réel les likes, tweets, etc. associés à un compte sur un réseau social. On peut voir cela comme une tangibilisation de l’espace non-tangible des réseaux sociaux.

![Smiirl - compteur connecté](images/smiirl--facebook.jpg)

[Voir en ligne](https://smiirl.com)

##### TexTuring



### à quoi ça pourrait vous servir dans votre scolarité ?

#### discussion sur la base des réponses des étudiants

### quels enjeux pour la programmation

#### lecture du texte code = design

#### enjeux :

##### empreinte de l’outil et uniformisation de la production

###### utiliser un langage de programmation pour utiliser différemment le logiciel (basil.js)

###### créer ses propres outils avec leur empreinte propres (TexTuring)

##### Manipuler de l’information et de la complexité

###### l’ordinateur peut faire facilement des choses pour nous difficile : se répeter, traiter beaucoup de données (Google Vol. 1)

##### tester ses suppositions et explorer les possibles avec des prototypes

###### design itératif

### Concrêtement, on va utiliser quoi dans l’atelier programmation

#### Les logiciels

##### la base : l’éditeur de texte

###### coloration syntaxique

###### numérotation des lignes

###### auto-complétion

##### les environnements de programmation

###### processing

> note : dispose de son propre éditeur

###### arduino
> note : dispose aussi de son propre éditeur

###### pure-data

###### Suite Adobe, logiciels 3D, web

#### les langages de programmation

#### le matériel

##### Arduino


###### introduction

Arduino est un projet développé en Italie dans le but de rendre le prototypage plus facile, rapide et abordable.

Il s’agit de cartes programmables qui peuvent gérer des entrée et sorties.

###### entrées, sorties

On peut donc avoir de l’information qui rentre et de l’information qui sort.

###### capteurs actionneurs

Les capteurs produisent de l’information, les actionneurs la transforme.

####### exemples de capteurs

* petite revue des capteurs de l’atelier
* capteurs de luminosité
* capteurs de présence
* capteurs de température
* caméra vidéo
* etc.

####### exemple d’actionneurs

* petite revue des actionneurs de l’atelier
* moteur continu
* servomoteurs
* etc.

###### exemple

exemple du montage lumineux / servo

Ici on a un montage très simple : en entrée en capte la luminosité, en sortie on actionne un servomoteur

Montrer le code

##### Raspberry Pi

##### autres

### Comment et où se former, à quoi faire attention

### Conclusion

http://blockly-games.appspot.com/

https://studio.code.org/s/artist/stage/1/puzzle/1
