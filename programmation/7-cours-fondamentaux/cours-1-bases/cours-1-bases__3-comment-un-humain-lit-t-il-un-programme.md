# Comment un humain lit-il un programme ?

Un programme n’est pas destiné à n’être lu que par un ordinateur. Le programmeur sera amené à se relire ainsi qu’à relire des programmes écrits par d’autres. Et il est très commun aujourd’hui d’écrire des programmes collaborativement. On écrit donc déjà pour soi, mais aussi pour les autres.

Il est très important de produire un code clair et facile à comprendre.

## Des outils pour bien écrire

Pour maintenir un code lisible et compréhensible, le programmeur a à sa disposition différents outils.

Pour démontrer cela, nous allons prendre l’exemple suivant, volontairement extrêmement mal rédigé :

```javascript
function a(aa){var aaa=Math.random();
if(aaa<aa){return('pile');}
  else{return 'face';}
}console.log(r(0.5)));
```

### espaces et retours à la ligne

Comme lorsque l’on écrit n’importe quel document, on peut en espacer les éléments pour amener des respirations dans le document. Cela en utilisant des retours à la ligne ou des caractères espace.

```javascript
function a ( aa ) {
var aaa = Math.random();
if ( aaa < aa ) {
return('pile');
} else {
return 'face';
}
}

console.log( r( 0.5 ) );
```

## retraits

Les retraits sont utilisés en programmation pour délimiter visuellement des blocs logiques de code ainsi que leur hiérarchie.

```javascript
function a (aa) {
  var aaa = Math.random();
  if ( aaa < aa ) {
    return('pile');
  } else {
    return 'face';
  }
}

console.log( r( 0.5 ) );
```

### identificateurs

Les identificateurs doivent être clairs, fournir une information sur leur contenu et usage.

```javascript
function pile_ou_face ( probabilite_pile ) {
  var nombre_au_hasard = Math.random();
  if ( nombre_au_hasard < probabilite_pile ) {
    return('pile');
  } else {
    return 'face';
  }
}

console.log( pile_ou_face(0.5) );
```
### commentaires

Les commentaires sont là pour fourni des informations, explications au lecteur afin de lui faciliter la compréhension de ce qu’il lit.


```javascript
function pile_ou_face ( probabilite_pile ) {
  /* Renvoie 'pile' ou 'face' avec une probabilité donnée
  Nombre (de 0 à 1) -> Chaîne de caractères */

  // on prend un nombre aléatoire entre 0 et 1
  var nombre_au_hasard = Math.random();

  // s’il est inférieur à la probabilité donnée, on renvoie pile, et sinon face
  if ( nombre_au_hasard < probabilite_pile ) {
    return('pile');
  } else {
    return 'face';
  }
}

console.log( pile_ou_face(0.5) );
```

### choix stylistiques et constance

Il n’existe pas un seul style de rédaction, et les choix peuvent différer d’un programmeur à un autre. Certains styles sont encouragés par le langage lui-même, mais il est surtout important d’être constant, et de ne pas mélanger différents styles au sein d’un même projet.

#### exemple

Il existe plusieurs styles concernant les identificateurs, en voici deux :

```javascript
// camelCase
var monIdentificateur;

// snake_case
var mon_identificateur;
```

> Le camelCase et le snake_case sont deux types de notations. Le camelCase est plus courant dans certains langages comme le JavaScript, tandis que le snake_case sera plus communément adopté dans d’autre comme Python. Mais il est tout à fait possible de faire ses propres choix et, par exemple, d’utiliser le snake_case en JavaScript.

### les guides de style

Afin d’harmoniser les styles de programmation au sein d’un groupe de travail, on peut éditer ce que l’on appelle un « guide de style », qui va regrouper un ensemble de choix stylistiques qui devront être suivis par tout l’équipe afin de garder un code homogène.

De nombreux guides sont disponibles en ligne et permette de produire plus facilement un code lisible ou d’adopter de bonnes pratiques.

### Les recommandations du langage

Les organisations qui se chargent de définir ou maintenir un langage peuvent émettre des recommandations, qui n’ont toutefois pas valeur d’obligation, même si elles peuvent être très communes parmi les utilisateur de ce langage.
