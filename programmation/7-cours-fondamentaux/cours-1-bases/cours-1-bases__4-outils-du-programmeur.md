# Les outils du programmeur

## Les documentations

Définition :

> Une documentation est un ensemble de documents recueillis sur une question. (source : [wiktionary](https://fr.wiktionary.org/wiki/documentation))

En programmation, les documentations sont des bases documentaires. Elles visent à donner aux personnes pratiquant la programmation un accès facile à toute sorte d’information en rapport avec un langage ou outil.

La documentation est l’outil le plus important pour programmer : il est bien plus problématique d’avoir une mauvaise documentation, qu’un mauvais éditeur de texte !

Plusieurs types de documentations peuvent exister, avec des usages différents. Parmi lesquels :

* la spécification d’un langage
* la ou les documentations de référence
* les documentations indépendantes
* les guides de style
* les articles, sheets, guides, ouvrages

### La spécification d’un langage

La spécification d’un langage est l’ensemble de normes définissant le langage lui-même. C'estun document très précis et complexe, qui est surtout utilisé pour *implémenter* le langage, c’est-à-dire créer le logiciel qui va pouvoir lire les programmes.

c’est un document très spécifique que l’on n’utilise en général jamais ou très rarement lors d’une recherche d’information.

Un exemple de spécification est le standard [ECMA-262](https://tc39.github.io/ecma262/) de l’ECMAScript (le nom JavaScript n’étant en réalité que le nom commercial de l’ECMAScript).

### La documentation de référence

Un langage peut disposer d’une documentation de référence, maintenue par l’organisation ou les personnes responsables du développement du langage.

c’est le cas, entre autres, du [langage Python](https://docs.python.org/), ou du [logiciel Blender](https://www.blender.org/manual/).

On peut parler de documentation de référence (*documentation*), de référence du langage (*reference documentation*), de manuel de référence (*reference manual*), etc.

### les documentations indépendantes

Il peut arriver que les organisations développant un langage n’éditent pas elles-mêmes de documentation considérée comme adaptée à un usage courant. C'estnotamment le cas pour les technologies du web, gérées par des consortiums industriels comme le W3C pour le CSS et le HTML, ou Ecma International pour le JavaScript.

Dans ces cas, on peut se rediriger vers des documentations, souvent collaboratives, qui vont remplir ce rôle.

c’est par exemple le cas des documentations du [Mozilla Developper Network](https://developer.mozilla.org) (MDN), qui couvrent entre autres CSS, HTML et JavaScript. Bien que cette documentation ne soit pas **la** référence car indépendante du W3C, cela reste une des documentations de référence.

### Les guides de style

l’importance de la constance lors de la rédaction de programme a été évoquée précédemment.

Un guide de style définit un ensemble de recommandations et bonnes pratiques quant à la manière de rédiger un programme. Celui-ci peut être rédigé et utilisé au sein d’une organisation, d’une équipe ou d’un projet. Il permet d’améliorer la constance et la qualité générale des programmes.

Consulter des guides de styles peut être un moyen d’évaluer les différentes manières de formuler des idées en programmes, et de faire ses propres choix. Il peut aussi permettre de s’informer sur ce qui est considéré comme une bonne ou une mauvaise pratique.

Pour quelqu’un dont le rôle ou la vocation n’est pas de faire ces choix de style, ce sont des moyens rapides et faciles d’adopter un style cohérent et efficace.

### Les articles, guides, ouvrages

## l’éditeur de texte

### Le complètement automatique (*autocomplete*)

Le complètement automatique (l’anglicisme *complétion automatique* est aussi utilisé) est un des outils très pratiques pour programmer.
