# objectif du cours

l’objectif de ce premier cours va être de s’intéresser au langage lui-même, ses éléments constitutifs. Comment les catégoriser, les nommer en français et en anglais. Comment écrit-on un programme ?

À la sortie du cours, vous aurez les éléments pour :
  * comprendre
  * catégoriser et nommer les éléments d’un programme
  * comprendre les différents types de documentations relatives à un langage de programmation
  * comprendre les enjeux de la lisibilité d’un programme et les outils à disposition du rédacteur d’un programme

Nous ferons à la fin du cours une petite présentation des principales technologies du web afin de préparer les prochains cours.

## objectifs : compréhensions, apprentissage et connaissances

| Compréhensions à long terme (comprendre que...)  | Objectifs d’Apprentissage (être capable de...) | Connaissances Essentielles (savoir que...) |
| --- | --- | --- |
| **C-1.1** Un programme est écrit dans un langage formel soumis à des règles lexicales et syntaxiques. | **OA-1.1.1** Décomposer un programme en unités lexicales. | **CE-1.1.1A** Un programme est composé d’**unités lexicales** appelées **jetons**. |
| | | **CE-1.1.1B** Le processus permettant le passage d’une suite de caractères dépourvue de sens à un ensemble de jetons signifiants et catégorisés est appelé **analyse lexicale**. |
| | | **CE-1.1.1C** Le processus permettant de déterminer la signification d’un ensemble de jetons est appelé **analyse syntaxique**. |
| | | **CE-1.1.1D** Différents langages peuvent classifier différemment les jetons, et la classification peut être très complexe. Toutefois, on peut globalement classifier les jetons en cinq catégories : **identificateurs**, **opérateurs**, **séparateurs**, **littéraux** et **commentaires**. |
| | | **CE-1.1.1E** Un jeton de type **identificateur** associe un nom à une entité. |
| | | **CE-1.1.1F** Un langage définit un nombre limité d’identificateurs particuliers appelés **mot-clés** ou **mots-clés réservés**. Il n’est généralement pas possible d’utiliser ces identificateurs pour autre chose que ce pour quoi ils ont été définis par le langage. |
| | | **CE-1.1.1G** Un langage peut définir des identificateurs correspondant à des valeurs ou fonctionnalités qui ne font pas partie du coeur du langage. |
| | | **CE-1.1.1H** Le développeur peut aussi déclarer des identificateurs afin de faciliter son travail. |
| | | **CE-1.1.1I** Un jeton de type **opérateur** opère sur des arguments et renvoie une valeur. |
| | | **CE-1.1.1J** Un jeton de type **séparateur** est une séquence d’un ou plusieurs caractères qui permet de délimiter ou séparer différentes régions indépendantes d’un programme. |
| | | **CE-1.1.1K** Un jeton de type **littéral** correspond à une valeur présente littéralement dans le code source d’un programme. |
| | | **CE-1.1.1L** Un jeton de type **commentaire** contient des indications à l’intention d’un lecteur humain. |
| | | **CE-1.1.1M** Les règles lexicales et syntaxiques peuvent varier d’un langage à un autre, tout comme elles peuvent être très proches voire identiques. Les mots-clés réservés, commentaires et séparateurs peuvent donc différer ou non. |
| | | **CE-1.1.1N** Des jetons mal formés peuvent empêcher l’exécution d’un programme, ou entraîner des résultats non désirés.On parle d’**erreurs lexicales**. |
| | | **CE-1.1.1O** Une séquence de jetons mal formée entraînera des erreurs de syntaxe qui peuvent empêcher l’exécution d’un programme, ou entraîner des résultats non désirés. On parle d’**erreur de syntaxe**. |
| **C-1.2** La présentation d’un programme est importante pour offrir lisibilité et compréhensibilité. | | **CE-1.2A** Il est très courant pour un programmeur d’avoir à relire son code ou celui d’un autre. La rédaction collaborative est aussi très répandue. Cela fait de la lisibilité et de la compréhensibilité des enjeux très importants. |
| | | **CE-1.2B** l’usage d’espaces et de retours à la ligne permet de délimiter les parties d’un programme et d’augmenter sa lisibilité. |
| | | **CE-1.2C** l’usage de retraits permet de hiérarchiser les composants d’un programme et d’augmenter sa lisibilité et sa compréhensibilité. |
| | | **CE-1.2D** l’utilisation d’identificateurs explicites est indispensable à la compréhensibilité d’un programme. |
| | | **CE-1.2E** l’insertion de commentaires est indispensable à la compréhensibilité d’un programme. |
| | | **CE-1.2F** Il n’existe pas un seul style de rédaction. Au rédacteur de faire ses choix. |
| | | **CE-1.2G** La constance stylistique lors de l’écriture d’un programme est indispensable à sa lisibilité. |
| | | **CE-1.2H** Les guides de style sont des outils pouvant permettre de définir et conserver un style homogène au sein d’un projet ou d’un groupe de travail. |
| **C-1.3** Plusieurs sources de documentation peuvent exister pour un même langage | | **CE-1.3.1A** Un développeur peut se référer à des sources de documentations lors de l’écriture de programme. |
| | | **CE-1.3.1B** Un langage peut être implémenté grâce à une **spécification** composée de **normes**. |
| | | **CE-1.3.1C** Une spécification est une documentation dont le but est de permettre l’implémentation du langage. Elle n’est pas conçue comme une documentation de tous les jours pour développer dans un langage donné. |
| | | **CE-1.3.1D** Des **documentations de référence** sont conçues pour permettre aux développeurs de trouver facilement les informations dont ils ont besoin pour programmer. |
| | | **CE-1.3.1E** Le développeur peut se référer à, ou développer, un **guide de style** afin de maintenir une constance stylistique au sein d’un projet. |
| | | **CE-1.3.1E** Un développeur doit faire attention à ses sources de documentation. Sont-elles à jour ? Les informations sont-elles correctes et complètes ? |
