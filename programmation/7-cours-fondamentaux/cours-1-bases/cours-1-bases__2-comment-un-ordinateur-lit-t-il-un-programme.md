# Comment un ordinateur lit-il un programme ?

Nous allons voir ici, grossièrement, comment un ordinateur passe d’une suite de caractères à un programme exécutable.

Le but est de se faire une idée de ce processus, de mettre en évidence les notions d’erreurs lexicales et syntaxiques, ainsi que de voir le vocabulaire permettant de désigner les unités de base d’un programme.

## I. analyse lexicale

> In computer science, **lexical analysis** is the process of converting a sequence of characters (such as in a computer program or web page) into a sequence of **tokens** (strings with an assigned and thus identified meaning).
>
> A program that performs lexical analysis may be termed a lexer, tokenizer, or scanner, though scanner is also a term for the first stage of a lexer.
>
>A lexer is generally combined with a parser, which together analyze the syntax of programming languages, web pages, and so forth.

[source : en.wikipedia.org/wiki/Lexical_analysis](https://en.wikipedia.org/wiki/Lexical_analysis)

> En informatique, **l’analyse lexicale** est le processus de conversion d’une séquence de caractères (par exemple un programme ou d’une page web) en une séquence de **jetons** (des chaînes de caractère auxquelles sont assigné une signification et donc un sens précis).
>
> Un programme qui procède à cette analyse peut être appelé un **analyseur lexical** (en anglais **lexer**, **tokeniser**, ou **scanner**, bien que scanner soit aussi le terme pour la première étape opérée par un analyseur lexical).
>
> Un analyseur lexical est en général combiné avec un analyseur syntaxique, lesquels analysent ensemble la syntaxe de langages de programmation, page webs, etc.

Il s’agit donc de passer d’une suite d’éléments 'caractères’, sans signification particulière en tant que tels, à une suite de 'jetons’, dont le type est connu et signifiant. On pourrait faire une analogie avec le passage pour l’humain des lettres aux mots :

![schéma des lettres aux mots catégorisé - analogie](images/analyse-lexicale--texte--1--coupee.png "une phrase")

![schéma des lettres aux mots catégorisé - analogie](images/analyse-lexicale--texte--2--coupee.png "les caractères")

![schéma des lettres aux mots catégorisé - analogie](images/analyse-lexicale--texte--3--coupee.png "les jetons")

![schéma des lettres aux mots catégorisé - analogie](images/analyse-lexicale--texte--4.png "les jetons catégorisés")

La même chose avec un programme :

![schéma des lettres aux mots catégorisé - analogie](images/analyse-lexicale--code--1.png "une phrase")

![schéma des lettres aux mots catégorisé - analogie](images/analyse-lexicale--code--2.png "les caractères")

![schéma des lettres aux mots catégorisé - analogie](images/analyse-lexicale--code--3.png "les jetons")

![schéma des lettres aux mots catégorisé - analogie](images/analyse-lexicale--code--4.png "les jetons catégorisés")

### catégories de jetons

Différents langages auront différentes catégories de jetons. On va ici prendre un modèle fictif (quoique extrêmement proche de celui de [Python]((https://docs.python.org/3.6/reference/lexical_analysis.html#other-tokens) par exemple) :

* ```identifier``` (identificateur) : des noms déterminés par le langage ou par le programmeur.
  * ```keyword``` ou ```reserved keyword``` (mot-clé réservé) : les noms définis par le langage sont dits réservés, ils ne peuvent pas être utilisés pour un autre usage que celui défini par le langage.
* ```operator``` (opérateur) : des symboles qui opèrent sur des arguments et produisent une valeur.
* ```delimiter``` ou ```punctuator``` (séparateur) : caractères permettant de délimiter différentes parties indépendantes d’un flux de données.
* ```literal``` (littéral) : une représentation d’une valeur directement dans le code source.
* ```comment``` (commentaire) : il s’agit d’indications, de documentation à l’intention du lecteur humain de programme (qui peut être le programmeur lui-même).

#### Les identificateurs, ou identifiants (*identifiers*)

>In computer science, identifiers (IDs) are lexical tokens that name entities. ([source : Wikipédia](https://en.wikipedia.org/wiki/Identifier#In_computer_science))

> En informatique, les identificateurs sont des jetons lexicaux qui nomment des entités.

Les **identificateurs** sont des noms, mots déterminés par le langage ou par l’utilisateur.

l’utilisateur peut s’en servir, par exemple, pour associer un nom facilement mémorisable à une valeur en mémoire.

**Exemple**

```javascript
// créé la variable nombre_aleatoire
// et lui assigne un nombre aléatoire entre 0 et 1
var nombre_aleatoire = Math.random();
```
> ```var``` est un identificateur défini par le langage. On parle alors de **mot-clé** où de **mot-clé réservé**.
>
> ```nombre_aleatoire``` est un identificateur défini par l’utilisateur
>
> ```Math``` est un identificateur correspondant à un **objet** présent dans l’environnement, fourni avec le langage.
>
> ```random``` est un identificateur correspondant à une **procédure** contenue dans l’objet ```Math```.

Les mots-clés du langage ne sont généralement pas utilisables comme identificateurs. Ils sont généralement d’un nombre limité.

**Exemple : [liste des 33 mots-clés réservés en ECMAscript 2015](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Lexical_grammar#Keywords) (version actuelle de la spécification correspondant au JavaScript utilisé dans les navigateurs)**

| liste |
| --- |
| break | case | catch | class | const |
| continue | debugger | default | delete | do |
| else | export | extends | finally | for |
| function | if | import | in | instanceof |
| new | return | super | switch | this |
| throw | try | typeof | var | void |
| while | with | yield |

> Voir aussi : [liste des objets fournis avec le langage](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects)

#### Les opérateurs (*operators*)

Les opérateurs sont des symboles qui opèrent sur des arguments et produisent une valeur. On parle d’**opérateur unaire** si celui-ci ne prend qu’une valeur, d’**opérateur binaire** s’il en prend deux, ou d’**opérateur ternaire** s’il en prend trois.

**Exemples**

```javascript
// opérateurs binaires
1 + 1 // renvoie 2
2 - 1 // renvoie 1

// opérateur unaire
n++ // renvoie n + 1 et assigne à n la valeur n + 1

// opérateur ternaire

// a ? b : c ;
0 > 1 ? 'si vrai' ? 'si faux' ; // si a est vrai, renvoie b, sinon renvoie c
```

#### Les séparateurs (*delimiters* ou *punctuators*)

> A **delimiter** is a sequence of one or more characters used to specify the boundary between separate, independent regions in plain text or other data streams. [source : Wikipédia](https://en.wikipedia.org/wiki/Delimiter)

> Un **séparateur** est une séquence d’un ou plusieurs caractères utilisés pour spécifier les limites entre des régions séparées, indépendantes d’un texte brut ou d’un autre flux de données.

```javascript
var ville = 'Stockholm';
console.log(ville);
```
> Plusieurs séparateur sont présents ici :
> Le point-virgule, qui permet de délimiter des instructions
> les apostrophes droites permettant de délimiter la chaîne de caractère ```Stockholm```
> le point permettant de séparer les identificateurs ```console``` et ```log```
> les parenthèses, permettant de délimiter une zone ou placer des arguments.

Les séparateurs (*delimiters* ou *punctuators* en anglais) sont des signes, qui

#### Les littéraux

> In computer science, a literal is a notation for representing a fixed value in source code. ([source : Wikipédia](https://en.wikipedia.org/wiki/Literal_%28computer_programming%29))

> En informatique, un littéral est une notation représentant dans le code source une valeur fixe.

Un littéral est une valeur présente littéralement dans un code source.

##### Exemples

```javascript
var ville = 'Stockholm';
var nombre_d_habitants = 914909;
```

> ici ```var```, ```ville```, ```nombre_d_habitants``` sont des **identificateurs**, ce sont des *index* qui renvoient à autre chose.
> Par contre, ```Stockholm``` et ```914909``` sont des **littéraux** : ils valent pour eux-même.

Par analogie :

```
Pierre dit 'bonjour !'.
```
> ici ```Pierre``` est un **identificateur** : c’est un *index* qui pointe vers une personne
> ```bonjour !``` est un **littéral**: c’est *littéralement* ce que dit Pierre

#### Les commentaires (*comments*)

> In computer programming, a comment is a programmer-readable explanation or annotation in the source code of a computer program. They are added with the purpose of making the source code easier for humans to understand, and are generally ignored by compilers and interpreters. ([source : Wikipédia](https://en.wikipedia.org/wiki/Comment_%28computer_programming%29))

> En programmation, un commentaire est une explication ou annotation dans le code source d’un programme, lisible par un programmeur. Ils sont ajoutés avec le but de rendre le code source plus facile à comprendre par les humains, et sont généralement totalement ignorés par les compilateurs et interpréteurs.

##### exemples

###### en JavaScript

```javascript
// commentaire sur une ligne

/* commentaire
  sur plusieurs lignes
  (multilignes) */
```

###### en Python

```python
# commentaire sur une ligne

""" commentaire
    sur plusieurs lignes
    (multilignes) """
```

### Les erreurs de nature lexicale

Un premier type d’erreur peut à ce stade être anticipée : les erreurs dues à des erreurs dans l’écriture des jetons, qui seront alors mal analysés.

#### Exemples

```javascript
// mal écrit
ver wille ='Stoekolm;
```
```javascript
// bien écrit
var ville = 'Stockholm';
```
> Plusieurs erreurs sont présentes dans le premier exemple :
> * ```ver``` au lieu de ```var``` : le mot-clé réservé ne sera pas reconnu
> * ```wille``` au lieu de ```ville``` ne provoquera pas d’erreur lors de l’analyse lexicale, cela reste un identificateur valide, mais cela pourrait entrainer des dysfonctionnements lors de l’exécution.
> * l’absence d’espace entre ```=``` et  ```'``` ne provoquera pas non plus d’erreur d’analyse lexicale. C'estpar contre une inconsistance de style qui peut gêner la lecture par un humain.
> * De même, l’erreur d’orthographe de ```Stockholm``` ne perturbera pas l’analyseur lexical
> * Par contre, l’oubli d’un des délimitateurs ```'``` provoquera des erreurs, l’analyseur lexical ne pouvant alors pas déterminer les limites du littéral ```Stockholm```.

On peut comparer ces erreurs à des erreurs d’orthographe : mal écrire les mots d’un programme peut amener à des erreurs.

## II. analyse syntaxique

l’analyse syntaxique permet quant à elle de déterminer le sens des instructions formées grâce aux jetons.

### Les erreurs de nature syntaxique

Une organisation précise de jetons correspond à un sens précis, et des jetons mal organisés peuvent engendrer un comportement non désiré du programme, un plantage voire une impossibilité pure et simple de procéder à l’analyse syntaxique.

### exemples

```javascript
// non
; ville = var 'Stockholm'

// oui
var ville = 'Stockholm';
```

Pour savoir comment organiser les jetons, on se réfère généralement à la documentation du langage.
