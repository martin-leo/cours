/lexique -> le lexique en/fr
/cours -> le cours
  / introduction à la programmation en école d’art/design
  / fondamentaux pour la programmation et les technologies du web




# Cours 1 : généralités






## Plan du cours

* objectif du cours
* comment un ordinateur lit-il un programme ?
  * résumé
  * analyse lexicale
    * le processus
    * les différents jetons
    * les mots-clé réservés
    *
  * analyse syntaxique
  * Où est défini le langage ? Où se documenter ?
* comment un humain lit-il un programme ?
  * bien écrire : outils et conventions
    * conventions
    * retraits
    * espaces
    * commentaires
    * identificateurs
  * documentation
    * normes
    * documentations
* vocabulaire additionnel
  * symboles
* HTML/CSS/JavaScript
  * usages
  * html
  * css
  * JavaScript
* Quelques outils
  * éditeurs de texte et IDE
  * le terminal
  * la gestion de versions

## objectifs des différents cours

Nous avons donc sept heures de cours ensemble. Sept heures, c’est assez court.

l’objectif ici ne va pas être de vous apprendre à programmer en soi. Il va davantage s’agir de vous donner des bases essentielles, une sorte de culture générale, qui vous aidera par la suite si vous êtes confronté à cet outil qu’est la programmation, que vous soyez la personne qui développe ou non.

Aujourd’hui, il est très facile de trouver de l’information en ligne, et de se former. Mais il existe certains écueils :
- les bases théoriques sont peu ou pas traitées, au profit d’une pratique immédiate. Ce qui entraîne des lacunes de vocabulaire et de compréhension (le comment plutôt que le pourquoi).
- on se forme à une technologie en particulier et pas aux technologies en général
- certains sujets sont si partiellement traités qu’ils poussent vers de mauvaises pratiques. En clair, on vous explique partiellement comment se servir d’un outil, et vous vous retrouver à mal l’utiliser, à prendre de mauvaises habitudes.

Les objectifs vont correspondre à cela :
- vous donner des bases lexicales et conceptuelles
- compléter certaines informations disponibles dans les formations en ligne avec des éléments trop souvent manquants alors qu’indispensables (selon moi), afin de vous éviter de faire n’importe quoi.

Deux autres objectifs vont être poursuivis :
- vous donner une vue d’ensemble des technologies webs et des outils
- vous donner de bonnes adresses pour vous autoformer.

Ce qui n’est pas un objectif :
- faire une redite des formations en ligne.

## vue d’ensemble du programme des 7 cours

  1. vue d’ensemble / bases de langage
  2. HTML
  3. CSS
  4. Programmation 1 : bases
  5. Programmation 2 : ?
  6. Programmation 3 : ?
  7. Programmation 4 : ?

. : opérateur ou délimiteur en ada ça semble être un delimiter https://en.wikibooks.org/wiki/Ada_Programming/Delimiters/dot
= : opérateur ? il semblerait
