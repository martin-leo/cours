# annexe : lexique

## analyse lexicale, syntaxique et jetons

| terme | traduction | signification |
| --- | --- | --- | --- |
| analyse lexicale | lexical analysis | signification |
| analyse syntaxique | syntaxic analysis | signification |
| jeton | token | signification |
| analyseur lexical | lexer, tokeniser, scanner | signification |
| analyseur syntaxique | parser | signification |
| identificateur ou identifiant | identifier | signification |
| mot-clé réservé ou mot-clé | reserved keyword ou keyword | signification |
| opérateur | operator | signification |
| séparateur | delimiter ou ponctuator | signification |
| littéral | literal | signification |
| commentaire | comment | signification |
| opérateur unaire | unary operator | signification |
| opérateur binaire | binary operator | signification |
| opérateur ternaire | ternary operator | signification |
| commentaire (sur une seul ligne) | single-line comment, line comment, inline comment | signification |
| commentaire multiligne | multiline comment, block comment | signification |

## caractères particuliers

Certains caractères particuliers sont utilisés en programmation, et il est important de bien savoir les nommer et les reconnaître, en français et en anglais.

| symbole | terme | traduction |
| --- | --- | --- | --- |
| { } | accolades | curly braces |
| [ ] | crochets | brackets |
| ( ) | parenthèses | parenthesis |
| < > | chevrons ouvrant et fermant | angle brackets |
| &#124; | barre verticale | pipe (tuyau) |
| & | esperluette | ampersand |
| : | deux-points | colon |
| ; | point-virgule | semicolon|
| , | virgule | period |
| ' | apostrophe droite | single quote |
| " | guillemets droits | double quote |
| - | tiret | dash |
| - | tiret bas, souligné | underscore |
| - | barre oblique | slash |
| - | barre oblique inversée | antislash |
| # | croisillon ou dièse  | hashtag |

## à trier


| terme | traduction | signification |
| --- | --- | --- | --- |
| guide de style | styleguide | signification |
| complètement automatique ou *complétion automatique* (anglicisme) | autocomplete | signification |
| terme | traduction | signification |
| terme | traduction | signification |
| terme | traduction | signification |

camelCase
snake_case
