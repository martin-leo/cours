ressources HTML

* W3C
  * https://www.w3.org/TR/html5/
  * validator.w3.org/
* WHATWG
  * https://html.spec.whatwg.org/
  * https://whatwg.org/
* Mozilla Developper Network : https://developer.mozilla.org/fr/docs/Web/Guide/HTML/HTML5
* HTML5 doctor : html5doctor.com

## plan du cours : ce qu’il faut savoir sur… le HTML

## HTML

HTML pour **HyperText Markup Language** soit **Langage de balisage Hypertexte**.

### Langage de balisage

> En informatique, les langages de balisage représentent une classe de langages spécialisés dans l’enrichissement d’information textuelle. Ils utilisent des balises, unités syntaxiques délimitant une séquence de caractères ou marquant une position précise à l’intérieur d’un flux de caractères (par exemple un fichier texte).

> l’inclusion de balises permet de transférer à la fois la structure du document et son contenu. Cette structure est compréhensible par un programme informatique, ce qui permet un traitement automatisé du contenu.

Source : [Wikipédia](https://fr.wikipedia.org/wiki/Langage_de_balisage)

Il s’agit donc :

* d’enrichissement de l’information textuelle
* à l’aide de balises qui, à l’intérieur d’un flux, délimitent le début et la fin d’un ensemble, ou marque sa position

#### exemple

![image non trouvée](images/exemple-html.png)

```
Mes supers vacances
Chère mamie, je suis en vacances à la mer, il y a des vagues.
Je te fais des gros bisous, Albert
Dessin représentant Albert à la plage. Il y a des vagues et un bateau.
Timbre à cent francs
Timbre à mille francs
Mamie
10 rue des arbres
CP 10000
trifouilly-les-oies
```

```html
<document>

  <!-- On peut avoir des métadonnées -->
  <metadonnees>
    <auteur>Albert</auteur>
    <type>carte postale</type>
  </metadonnes>

  <!-- Le contenu de la missive -->
  <contenu>
    <titre>Mes supers vacances</titre>
    <paragraphe>chère mamie, je suis en vacances à la mer, il y a des vagues.</paragraphe>
    <paragraphe>Je te fais des gros bisous, Albert</paragraphe>
    <image alt="Dessin représentant Albert à la plage. Il y a des vagues et un bateau." src="data:image/gif;base64,A0D8E56DFSQ0(…)"></image>
  </contenu>

  <!-- Affranchissement -->
  <affranchissement>
    <timbre valeur="100" devis="francs">
    <timbre valeur="1000" devis="francs">
  </affranchissement>

  <!-- La destination -->
  <destinataire>
    <nom>Mamie</nom>
    <adresse>
      <numero>10</numero>
      <rue>rue des arbres</rue>
      <cp>10000</cp>
      <localite>trifouilly-les-oies</localite>
    </adresse>
  </destinataire>

</document>
```

Ici l’on a des balises qui viennent délimiter des informations, comme celle du destinataire, de son nom, son adresse. On peut avoir des imbrications, du plus général au plus spécifique.

On a aussi des balises qui dénote simplement un élément comme pour les timbres.

On observe aussi la présence de couples attribut-valeurs qui sont un autre moyen d’ajouter de l’information.

### Hypertexte

> Un hypertexte est un document ou un ensemble de documents contenant des unités d’information liées entre elles par des hyperliens. Ce système permet à l’utilisateur d’aller directement à l’unité qui l’intéresse, à son gré, d’une façon non linéaire.

Note : l’idée d’hypertexte n’est pas nouvelle ([elle apparaît pour la première fois dans une publication en 1965](https://en.wikipedia.org/wiki/Hypertext#cite_note-5)) et peut-être retracée antérieurement à cela (est souvent cité l’article 'As We May Think' de l’ingénieur Vannevar Bush).

Ce qui est à retenir, c’est que le HTML permet aussi de lier des documents, ressources.

## Les représentations d’une structure HTML

### Le code Source

### Le DOM

### Le document outline

http://adrianroselli.com/2016/08/there-is-no-document-outline-algorithm.html

Le voir comme un outil pour bien définir son document ?


## Lexique

Moteur de rendu HTML - Layout Engine
