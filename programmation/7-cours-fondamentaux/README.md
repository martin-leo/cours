# 7 cours pour explorer des fondamentaux des technologies du web

## objectifs

l’objectif de ces cours n’est pas de prétendre former à la programmation, au HTML, au CSS. Il n’est pas de remplacer, ou de faire mieux que ce que les étudiants trouveront en ligne. En revanche, il s’agit de donner une vue d’ensemble de différents outils : HTML, CSS, JavaScript et la programmation en général. Il s’agit aussi de voir certains éléments fondamentaux utiles et trop souvent oubliés, ainsi que des bonnes pratique elles aussi oubliées, voire contredite, dans la plupart des ressources en ligne.

Cette suite de cours se veut un complément précédent l’apprentissage de l’étudiant, lui permettant d’avoir une vue d’ensemble, des fondamentaux, et des pistes afin d’apprendre par lui-même ce qui lui sera nécéssaire pour sa pratique personnelle du code.

## plan de cours

* cours 1 : vue d’ensemble et fondamentaux des langages
  * vue d’ensemble du cours : html, css, javaScript
  * comment un ordinateur lit-il un programme ? ```Comment un ordinateur passe-t-il d’une suite de caractères à un programme ? Quels sont les éléments qui constituent un langage ? Cette exploration des processus d’analyses lexicale et syntaxique constitue un prétexte permettant de s’intéresser aux plus petits éléments d’un programme et au vocabulaire relatif```
    * analyse lexicale ```analyse, lexèmes : ```
    * analyse syntaxique ```analyse```
* cours 2 : Ce qu’il faut savoir sur… HTML
  * un petit historique
    * origines
    * apparition
    * évolutions
    * aujourd’hui
      * acteurs
        * comment évolue le html ?
      * technologies
        * html5 'core'
        * api
  * bases de html
    * balises, type, attributs, valeurs
      * attributs communs
      * attributs spécifiques
      * data- at aria-
    * le html : la structure des documents
      * DOM, document outline
      * charge sémantique
      * éléments sectionnants
  * bonnes pratiques
* cours 3 : Ce qu’il faut savoir sur… CSS
  * petit historique
    * origines
    * apparition
    * évolutions
    * aujourd’hui
      * acteurs
        * comment évolue le html ?
      * technologies
        * html5 'core'
        * api
  * bases de CSS
    * sélecteurs, déclarations, propriétés, valeurs
    * spécificité
* cours 4 : Ce qu’il faut savoir sur… JavaScript
  *
* cours 5 : programmation :
  *
* cours 6 : programmation : environnements, mémoire, variables, structures de données
  *
* cours 7 : programmation : les outils du développeur
  * éditeur de texte
    * fonctionnalités
    * gestionnaire de versions
    * automatisation des tâches
