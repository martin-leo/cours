# Le DOM

Le HTML que nous écrivons n’est pas utilisé tel quel par le navigateur. Avant d’être utilisé, ce dernier est traité, et enrichi, par le moteur de rendu du navigateur.

Le moteur de rendu d’un navigateur peut changer d’un navigateur à l’autre, et il peut arriver que le rendu ne soit pas identique de l’un à l’autre.

![schéma source, dom, moteur de rendu](images/schema-html-navigateur.svg)

Le HTML lu par le navigateur est donc transformée en une arborescence appelé le DOM, pour **Document Object Model** où **Modèle d’Objet Document**.

## Exemple de représentation en arborescence

> Le schéma suivant représente notre carte postale sous forme d’arborescence

![schéma d’exemple](images/schema-carte-postale.svg)

> Pour rappel le document balisé était le suivant :

```html
<document>

  <!-- On peut avoir des métadonnées -->
  <metadonnees>
    <auteur>Albert</auteur>
    <type>carte postale</type>
  </metadonnes>

  <!-- Le contenu de la missive -->
  <contenu>

    <principal>
      <titre>Mes supers vacances</titre>
      <paragraphe>chère mamie, je suis en vacances à la mer, il y a des vagues.</paragraphe>
      <paragraphe>Je te fais des gros bisous, Albert</paragraphe>
      <image alt="Dessin représentant Albert à la plage. Il y a des vagues et un bateau." src="images/dessin-d-albert.png">
    <principal>

    <!-- Affranchissement -->
    <affranchissement>
      <timbre valeur="100" devise="francs">
      <timbre valeur="1000" devise="francs">
    </affranchissement>

    <!-- La destination -->
    <destinataire>
      <nom>Mamie</nom>
      <adresse>
        <numero>10</numero>
        <rue>rue des arbres</rue>
        <cp>10000</cp>
        <localite>trifouilly-les-oies</localite>
      </adresse>
    </destinataire>

  </contenu>  

</document>
```

## terminologie

Le DOM étant un élément central d’une structure HTML/CSS/JavaScript, il est important d’avoir une introduction minimale à sa terminologie.

### racine, noeuds, feuilles

Une arborescence est constitué d’éléments appelés noeuds (*nodes*). Elle à pour origine un noeud appelé racine (*root*), et est composé en ses extrémités de feuilles (*leaves*), c’est-à-dire de noeud ne contenant aucun autre noeud.

![schéma noeuds](images/schema-lexique-arborescence--noeuds.svg)

### Élément enfants, éléments parents

Si un noeuds contient un ou plusieurs noeuds, ceux-ci sont appelés noeuds enfants (*child*). Si un noeud est contenu par un autre, celui-ci est appelé noeud parent (*parent*).

![schéma noeuds enfants et parents](images/schema-lexique-arborescence--enfants-parents.svg)

### Éléments frères

Les noeuds partageant le même parent sont aussi appelés, relativement l’un à l’autre, noeuds frères (*siblings*).

![schéma noeuds frères](images/schema-lexique-arborescence--freres.svg)

### Ancètres et descendants

On appelle descendant (*descendant*) d’un noeud tous les noeuds qui descendent de celui-ci. De même, sont appelés ancètres (*ancestors*) tous les noeuds dont un noeud descend.

![schéma noeuds ancetres et descendants](images/schema-lexique-arborescence--ancetres-descendants.svg)
