# HTML : contexte

Le HTML est un langage utilisé, avec CSS et JavaScript, pour différents usages. Le plus connu est le web, mais ces langages sont aujourd’hui aussi utilisés pour conçevoir des applications qui dépassent celui-ci.

## Quelques exemples d’utilisation d’HTML, CSS et JavaScript

### site internet : wikipedia

![capture d’écran du site wikipedia.org](images/wikipedia.org.png)

### livres numériques

![capture d’écran du logiciel de lecture de livres numériques Calibre](images/calibre.png)

### application : Atom

![capture d’écran du logiciel ATOM](images/atom.png)

### application mobile

![captures d’écran d’une application mobile](images/application-ici-saint-denis.png)

## JavaScript

Le langage de programmation JavaScript dépasse aujourd’hui très largement son cadre initial. Il est par exemple présent dans les différents logiciel de la suite Adobe, utilisé côté serveur par de nombreux site internet, pour programmer des robots, et trouve même sa place pour des dispositifs et installations artistiques.

### Quelques exemples d’usage de JavaScript hors du net

#### ExtendScript

La suite de logiciel édité par la société Adobe utilise JavaScript, sous le nom d’ExtendScript, pour permettre de contrôler programmatiquement ses logiciels.

![Photographie du projet 1 second](images/twitter-1-second--0-global-view.jpg)

> Ci-dessus, le projet 1 second, qui rassemble tous les tweets posté à une seconde précise.

### Unity

Unity est un logiciel de conception de jeux vidéo, et permet de programmer des jeux en JavaScript.

### seriously.js

Un exemple d’usage inhabituel : du compositing en JavaScript : [seriouslyjs](http://www.seriouslyjs.org/).

#### node.js

Node.js est un environnement de programmation basé sur Javascript (dans sa version dite ES6). C'estune technologie qui à connu ses dernière années une si ce n’est la plus grosse croissance jamais vue. Et node est partout. Derrière les applications vues plus haut (ATOM et l’application mobile)

##### nodebots

Il est aujourd’hui possible, à l’aide de logiciels comme Johnny-Five, de contrôler en Javascript des microcontroleur comme Arduino, et de s’en servir pour programmer, entre autre, des robots.

> Cela est particulièrement intéressant, car le langage utilisé par les cartes Arduino, le C, est un langage bien plus bas-niveau que le JavaScript. Le modèle événementiel du JavaScript peut aussi faciliter la programmation.
> Il faut toutefois bien noter qu’il s’agit bien souvent pas de contrôler le microcontrôleur en JavaScript (le Javascript étant exécuté sur un ordinateur connecté au microcontrôleur) et non de programmer le microcontrôleur en JavaScript (Le microcontrôleur ne peux exécuter du JavaScript).

#### installation artistique

http://www.creativeapplications.net/?s=nodejs

### JavaScript : conclusion

JavaScript est partout, apprendre JavaScript est loin de n’être utile que pour le web.

## un principe de conception : la séparation des préoccupations

Il existe en informatique un principe de conception (*design principle*) (parmi d’autres) appelé *"separation of concerns"* ou *"séparation des préoccupations"*.

Il s’agit de diviser un programme, système, dispositif... en différentes parties s’occupant chacune de quelque chose de précis.

Dans le cas du web côté client (celui qui consulte les documents, à opposer au côté serveur, qui va fournir lesdits documents), une expression de ce principe est le suivant :

* le langage de balisage (HTML) s’occupe de fournir la structure sémantique du document
* les feuilles de styles (CSS) s’occupent de déterminer la présentation (la mise en page) du document
* le langage de programmation (JavaScript) s’occupe de gérer l’interactivité du document

Ce qu’il faut en retenir, c’est que le HTML ne s’occupe ni de l’aspect visuel du document, ni de son interactivité. Et qu’il est préférable d’éviter, autant que faire se peut, de mélanger HTML, CSS et JavaScript.

## Des langages en évolution

### HTML et CSS : des standards en perpétuelle évolution

HTML et CSS sont ce que l’on appelle des *living standards*, c’est-à-dire qu’ils ne fonctionnent pas par version à proprement parler, mis sont en constante évolution. Ce qui s’en passe en gros, c’est qu’on a une technologie, HTML, CSS, des industriels (Google, Apple, Microsoft) qui formulent des propositions de fonctionnalités à un consortium (un regroupement d’industriels), le W3C, et les implémentent. Et ça finit, ou pas par être intégré formellement aux spécifications. Mais retenez qu’HTML et CSS sont donc des langages en perpétuelle évolution et qu’il faut donc perpétuellement suivre leurs évolutions.

Le site [caniuse.com](http://caniuse.com/) est un très bon outil pour avoir une idée de l’adoption d’une technologie, et donc de s’il est pertinent ou pas de l’utiliser sur le web.

Pour en savoir plus sur l’état actuel du web et comment ça se passe, je vous conseille la conférence de Daniel Glazman
["La troisième guerre des navigateurs est finie et c’est un bain de sang"](https://www.youtube.com/watch?v=ceMLuRBn--M), qui dure une quarantaine de minutes.

### JavaScript

JavaScript est un peu différent. Contrairement au HTML5/CSS qui continuent d’évoluer sans que cela définisse de nouvelle version, il existe plusieurs versions de JavaScript qui sont elles figées. Les navigateurs web implémentent actuellement tous la version dite "ES5", qui date de 2009. Le support de la version ES6, finalisée en 2015, est en cours d’implémentation. Il existe une version ES7 (juin 2016) et ES8 (qui à date de janvier 2017, est à l’état de *draft*);
