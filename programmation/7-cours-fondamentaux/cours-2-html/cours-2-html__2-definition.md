# HTML, définition

## définition

HTML pour **HyperText Markup Language** soit **Langage de balisage Hypertexte**.

### Hypertexte

> Un hypertexte est un document ou un ensemble de documents contenant des unités d’information liées entre elles par des hyperliens. Ce système permet à l’utilisateur d’aller directement à l’unité qui l’intéresse, à son gré, d’une façon non linéaire.

[source : Wikipédia](https://fr.wikipedia.org/wiki/Hypertexte)

![images extraites de la présentation du oN-Line System system par Douglas Engelbart (vidéo dite « Mother of all demos »)](images/engelbart-mother-of-all-demos-hypertext.jpg)

[Vidéo : extrait de la présentation du oN-Line System system par Douglas Engelbart (vidéo dite « Mother of all demos ») (1968)](https://www.youtube.com/watch?v=74c8LntW7fo)

Cette vidéo nous montre un système hypertexte précurseur, en 1968 (le terme hypertexte a quant à lui été inventé en 1963). d’autres systèmes ont existé, avant et après, et ont précédé l’invention du WorldWideWeb en 1990 par Tim Berners-Lee.

### Langage de balisage

>  En informatique, les langages de balisage représentent une classe de langages spécialisés dans l’enrichissement d’information textuelle. Ils utilisent des balises, unités syntaxiques délimitant une séquence de caractères ou marquant une position précise à l’intérieur d’un flux de caractères (par exemple un fichier texte).

>  l’inclusion de balises permet de transférer à la fois la structure du document et son contenu. Cette structure est compréhensible par un programme informatique, ce qui permet un traitement automatisé du contenu.

Source : [Wikipédia](https://fr.wikipedia.org/wiki/Langage_de_balisage)

On peut de cette définition ressortir plusieurs idées. Avec le HTML, on va :

* travailler avec de l’information textuelle
* enrichir ce texte, c’est-à-dire y ajouter de l’information
* utiliser des **balises** pour indiquer des positions ou délimitations au sein du texte

Autre élément important, on peut aussi noter que tout cela vise aussi à rendre ce contenu accessible à des programmes informatiques (par exemple des navigateurs, mais aussi des dispositifs plus indépendants, comme des moteurs de recherche).

![](images/moteur-de-recherche.png)

> Ci-dessus, on peut voir que le moteur de recherche présente un petit encart sur le sujet de la recherche. Il a été programmé pour rechercher automatiquement des informations sur Wikipédia et les afficher.

![](images/twitter-bot-parliamentedits.png)

> Ci-dessus, le robot Twitter @parliamentedits : celui tweete dès qu’une modification de Wikipédia est faite depuis des adresses IP correspondant à des parlementaires britanniques.

Et à nouveau : s’agissant ici de décrire la structure d’un document, et non sa présentation, on n’incluera aussi peu que possible d’information sur l’aspect visuel.

#### Exemple de balisage de document

Prenons cette carte postale :

![image non trouvée](images/exemple-html.png)

Convertissons-la en texte :

```
Mes supers vacances
Chère mamie, je suis en vacances à la mer, il y a des vagues.
Je te fais des gros bisous, Albert
Dessin représentant Albert à la plage. Il y a des vagues et un bateau.
Timbre à cent francs
Timbre à mille francs
Mamie
10 rue des arbres
CP 10000
trifouilly-les-oies
```

l’essentiel, le contenu, est là, mais il va être difficile d’exploiter cela avec un système informatisé.
Maintenant, regardons à quoi cela pourrait ressembler en ajoutant de l’information avec un langage de balisage similaire au HTML :

```html
<document>

  <!-- On peut avoir des métadonnées -->
  <metadonnees>
    <auteur>Albert</auteur>
    <type>carte postale</type>
  </metadonnes>

  <!-- Le contenu de la missive -->
  <contenu>

    <principal>
      <titre>Mes supers vacances</titre>
      <paragraphe>chère mamie, je suis en vacances à la mer, il y a des vagues.</paragraphe>
      <paragraphe>Je te fais des gros bisous, Albert</paragraphe>
      <image alt="Dessin représentant Albert à la plage. Il y a des vagues et un bateau." src="images/dessin-d-albert.png">
    <principal>

    <!-- Affranchissement -->
    <affranchissement>
      <timbre valeur="100" devise="francs">
      <timbre valeur="1000" devise="francs">
    </affranchissement>

    <!-- La destination -->
    <destinataire>
      <nom>Mamie</nom>
      <adresse>
        <numero>10</numero>
        <rue>rue des arbres</rue>
        <cp>10000</cp>
        <localite>trifouilly-les-oies</localite>
      </adresse>
    </destinataire>

  </contenu>  

</document>
```
