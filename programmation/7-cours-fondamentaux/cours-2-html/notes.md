
## TODO

* vérifier son HTML : image pour l’affichage source
* voir s’il existe un autre outil dans l’inspecteur
*

partir du DOM pour aller vers comment le construire, avec les balises ? nope

## ressources HTML

* W3C
  * https://www.w3.org/TR/html5/
  * validator.w3.org/
* WHATWG
  * https://html.spec.whatwg.org/
  * https://whatwg.org/
* Mozilla Developper Network : https://developer.mozilla.org/fr/docs/Web/Guide/HTML/HTML5
* HTML5 doctor : html5doctor.com

## plan du cours : ce qu’il faut savoir sur… le HTML

## HTML

HTML pour **HyperText Markup Language** soit **Langage de balisage Hypertexte**.

### Langage de balisage

  >  En informatique, les langages de balisage représentent une classe de langages spécialisés dans l’enrichissement d’information textuelle. Ils utilisent des balises, unités syntaxiques délimitant une séquence de caractères ou marquant une position précise à l’intérieur d’un flux de caractères (par exemple un fichier texte).


  >  l’inclusion de balises permet de transférer à la fois la structure du document et son contenu. Cette structure est compréhensible par un programme informatique, ce qui permet un traitement automatisé du contenu.


Source : [Wikipédia](https://fr.wikipedia.org/wiki/Langage_de_balisage)

Il s’agit donc :

* d’enrichissement de l’information textuelle
* à l’aide de balises qui, à l’intérieur d’un flux, délimitent le début et la fin d’un ensemble, ou marque sa position

#### exemple

![image non trouvée](images/exemple-html.png)

```
Mes supers vacances
Chère mamie, je suis en vacances à la mer, il y a des vagues.
Je te fais des gros bisous, Albert
Dessin représentant Albert à la plage. Il y a des vagues et un bateau.
Timbre à cent francs
Timbre à mille francs
Mamie
10 rue des arbres
CP 10000
trifouilly-les-oies
```

```html
<document>

  <!-- On peut avoir des métadonnées -->
  <metadonnees>
    <auteur>Albert</auteur>
    <type>carte postale</type>
  </metadonnes>

  <!-- Le contenu de la missive -->
  <contenu>
    <titre>Mes supers vacances</titre>
    <paragraphe>chère mamie, je suis en vacances à la mer, il y a des vagues.</paragraphe>
    <paragraphe>Je te fais des gros bisous, Albert</paragraphe>
    <image alt="Dessin représentant Albert à la plage. Il y a des vagues et un bateau." src="data:image/gif;base64,A0D8E56DFSQ0(…)"></image>
  </contenu>

  <!-- Affranchissement -->
  <affranchissement>
    <timbre valeur="100" devis="francs">
    <timbre valeur="1000" devis="francs">
  </affranchissement>

  <!-- La destination -->
  <destinataire>
    <nom>Mamie</nom>
    <adresse>
      <numero>10</numero>
      <rue>rue des arbres</rue>
      <cp>10000</cp>
      <localite>trifouilly-les-oies</localite>
    </adresse>
  </destinataire>

</document>
```

Ici l’on a des balises qui viennent délimiter des informations, comme celle du destinataire, de son nom, son adresse. On peut avoir des imbrications, du plus général au plus spécifique.

On a aussi des balises qui dénote simplement un élément comme pour les timbres.

On observe aussi la présence de couples attribut-valeurs qui sont un autre moyen d’ajouter de l’information.

### Hypertexte

    Un hypertexte est un document ou un ensemble de documents contenant des unités d’information liées entre elles par des hyperliens. Ce système permet à l’utilisateur d’aller directement à l’unité qui l’intéresse, à son gré, d’une façon non linéaire.


Note : l’idée d’hypertexte n’est pas nouvelle ([elle apparaît pour la première fois dans une publication en 1965](https://en.wikipedia.org/wiki/Hypertext#cite_note-5)) et peut-être retracée antérieurement à cela (est souvent cité l’article 'As We May Think' de l’ingénieur Vannevar Bush).

Ce qui est à retenir, c’est que le HTML permet aussi de lier des documents, ressources.

## Les représentations d’une structure HTML

### Le code Source

### Le DOM

### Le document outline

http://adrianroselli.com/2016/08/there-is-no-document-outline-algorithm.html

Le voir comme un outil pour bien définir son document ?


## Lexique

Moteur de rendu HTML - Layout Engine












## notes

1990 cern
MOSAIC
95 sortie d’IE (la pire catastrophe de l’histoire d’internet) 1ère bataille des navigateurs (96-98)
2003 fin de netscape, du coup chez microsoft on se dit qu’on a plus besoin de mettre à jour parce qu’on est leader et qu’au fond le web on s’en tape tant qu’on a le marché jusqu’en 2008
années 2000 2ème bataille des navigateurs
années 2010 HTML5, domination Google

IE6 sort en 2001 et pas de maj avant 5 ans :
This version of Internet Explorer has been widely criticized for its security issues and lack of support for modern web standards, making frequent appearances in "worst tech products of all time" lists, with PC World labeling it "the least secure software on the planet."[2]

## actualité






Cette idée d’hypertexte n’apparaît pas avec le *World Wide Web*. On peut, en vrac, citer pour ceux que cela intéresse, le Memex de Vannevar Bush, décrit en 1945 dans l’article « How we may think », le projet Xanadu en 1960, par Ted Nelson, qui inventera les termes *Hypertexte* et *Hypermedia* en 1963. Il produira ensuite en 1967 le Hypertext Editing System qui sera utilisé par la NASA pour documenter le projet Apollo.


l’inventeur du HTML, Tim Berners-Lee, travaillera au sein du CERN sur le projet ENQUIRE, système hypertexte, en 1980, mais c’est en 1990 que le World Wide Web voit le jour, et avec lui, le HTML.





###

Le but du HTML n’est pas d’être affiché tel quel. Il n’est d’ailleurs pas question de l’aspect du document en HTML. C'esten effet généralement le rôle des feuilles de style

Alors bien sûr le but n’est pas que cette structure soit affichée telle qu’elle. Mais c’est un outil qui va nous permettre de traiter plus facilement cette information.

Il ne s’agit pas encore de s’occuper de comment l’information va être *présentée*. C'estlà la tâche de ce qu’on appelle les *feuilles de style* (connues sous leur acronyme **CSS**).



Ici l’on a des balises qui viennent délimiter des informations, comme celle du destinataire, de son nom, son adresse. On peut avoir des imbrications, du plus général au plus spécifique.

On a aussi des balises qui dénote simplement un élément comme pour les timbres.

On observe aussi la présence de couples attribut-valeurs qui sont un autre moyen d’ajouter de l’information.

## Rôle du HTML
