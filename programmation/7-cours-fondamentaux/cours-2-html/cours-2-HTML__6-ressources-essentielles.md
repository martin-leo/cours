# HTML : les ressources essentielles

* documentation de référence
  * [MDN : Mozilla Developper Network](https://developer.mozilla.org/fr/docs/Web/HTML/) (fr)
  * [HTML5 doctor](https://html5doctor.com) (en)
* outils
  * vérifier son HTML avec le [validateur HTML du W3C](validator.w3.org/)
* spécifications
  * [Spécification *Living standard* du WHATWG](https://html.spec.whatwg.org/) (en)
  * [recommandation du W3C](https://www.w3.org/TR/html/) (en)
