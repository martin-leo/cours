# langage HTML : les composants

## Les balises

### à quoi sert une balise ?

Une balise sert à délimiter ou positionner un élément HTML, les éléments HTML étant les composants des pages HTML. Elle contient aussi une charge sémantique, c’est-à-dire qu’elle permet d’ajouter de l’information sur l’usage ou le sens de cet élément.

### anatomie minimale d’une balise

Une balise est composée au minimum de :
* deux séparateurs qui dénotent le début et la fin de la balise. On utilise un chevron ouvrant pour indiquer le début de la balise, et un chevron fermant pour en indiquer la fin.
* un identificateur qui correspond au type de balise

#### quelques exemples

```html
<img>

<button>

<article>

<section>
```

### balises ouvrantes, balises fermantes, balises orphelines

#### balises ouvrantes, balises fermantes

Les types de balise les plus courantes sont les **balises ouvrantes** et les **balises fermantes**, qui fonctionnent toujours par deux. Elles servent à délimiter une zone dans le contenu.

La balise fermante possède une particularité : une barre oblique qui vient indiquer qu’elle vient fermer l’élément débuté par la balise ouvrante.

```html
<type> contenu </type>
```

> toujours bien penser à fermer une balise ouverte, sous peine de quoi l’on peut obtenir des résultats inattendus. Le HTML étant traité de manière très tolérante par les navigateurs, il n’est pas forcément visible qu’une erreur s’est dissimulée dans le code.

#### balise orpheline

Une balise orpheline est une balise qui va positionner un élément, et n’a donc pas besoin de définir un début ou une fin.

On peut prendre l’exemple de la balise ```<hr>``` (*horizontal rule*) qui indique la position d’un filet horizontal :

```html
<hr>
```

### balises à caractère sectionnants et non-sectionnants

> Attention : le caractère sectionnant n’est pas un à opposer au caractère ouvrant/fermant/orphelin des balises. Ces deux choses ne sont pas liées.

Un document HTML est, comme son nom l’indique, un document. À ce titre, il possède un *outline* (une table des matières) composée de différentes sections.

Prenons l’exemple fictif qui suit :

```html
<document>
  <h1>mon document</h1>

  <section>
    <h2>1. partie 1</h2>
  </section>

  <section>
    <h2>2. partie 2</h2>

    <section>
      <h2>2.1. partie 2.1</h2>
    </section>

    <div>Ceci est un élément non-sectionnant.</div>

  </section>

<document>
```

l’*outline* de ce document serait :

* mon document
  * partie 1
  * partie 2
    * partie 2.1

Certaines balises vont créer de nouvelles sections, d’autres non. C'estun des paramètres à prendre en compte lors de la structuration de votre document.

### Quelle balise choisir ?

Il existe [plus d’une centaine de balise différentes](https://developer.mozilla.org/fr/docs/Web/HTML/Element). Il n’est pas nécéssaire de connaître la liste par cœur, mais il est utile de se former à leur usage et de consulter une documentation pour faire son choix.

#### Choix fonctionnel

Certaines balises s’imposent uniquement par leur fonction. La balise image ```<img>``` par exemple, ou les balise liste ordonnée ```<ol>``` et liste non ordonnée ```<ul>```.

#### Choix sémantique

Pour d’autres balises, le choix est aussi sémantique. C'estpar exemple le cas, entre autres, des balises ```<article>``` ```<section>``` et ```<div>```. Ces balises ont la même fonction, très générique, qui est d’englober, de contenir une zone du document.

La balise ```<article>``` dénote un contenu constituant une unité pouvant être détachée du reste du document (par exemple un billet d’un blog, un produit sur un site de vente en ligne). C'estun élément qui fait toujours sens même extrait de son contexte.

La balise ```<section>``` dénote elle un contenu qui constitue une section dans le document. Il n’est pas certain que cet élément fasse toujours sens extrait de son contexte.

La balise ```<div>``` elle ne dénote rien du tout, et peut être utilisée par exemple pour grouper des éléments en vue de leur mise en page sans impacter la structure sémantique du document.

## attributs

Les attributs sont un moyen d’ajouter de l’information sur une balise. On peut associer aucun, un ou plusieurs attribut à un balise orpheline ou ouvrante.

> Un balise fermante ne reçoit jamais aucun attribut.

### anatomie des attributs

Un attribut est constitué au minimum d’un identificateur.

```html
<div hidden>
```
> Ici l’élément ```div``` est accompagné de l’attribut ```hidden``` qui est interprété par le navigateur comme l’indication qu’il doit cacher l’élément.

Plus souvent, un attribut est associé, à l’aide d’un symbole égal, à une chaîne de caractère entre guillemets droit.

```html
<img src="mon/dossier/image.jpg">
```

> On spécifie ici que l’on positionne un objet image qui permettra l’affichage du fichier source image.jpg contenu dans mon/dossier/

### les attributes possibles

#### attributs globaux

Les attributs globaux sont des attributs susceptibles d’être appliqués à n’importe quelle balise. [La spécification en indique 12](https://www.w3.org/TR/html5/dom.html#global-attributes). Cependant, les deux attributs globaux les plus courants sont les suivants :

##### id

l’attribut ```id``` permet de définir un identificateur unique pour un élément. Il permet notamment de sauter directement à une partie spécifique du document grâce à un lien hypertexte.

Les valeur d’```id``` ne sont pas définies par la spécification HTML. On tentera autant que possible d’indiquer un ```id``` qui nous renseigne sur l’élément sur lequel il est appliqué.

> Il s’agit d’un attribut unique, qui ne peut être attribué qu’une seule fois.

```html
<section id="exemple-d-id">contenu<section>

<a href="#exemple-d-id">lien vers l’exemple</a>
```

> Ici on définit un élément ```section```  avec l’identifiant unique ```exemple-d-id```

> l’élément ```a``` (pour *anchor*, ancre) est un lien qui renvoit vers l’adresse indiquée via son attribut ```href``` (pour *Hypertexte REFerence*). Ici, il renverra vers l’élément donc l’```id``` sera ```exemple-d-id```.

##### Class

l’attribut ```class``` permet de définir un ou plusieurs identificateurs, séparées par des espaces, et permettant d’indiquer les classes (on pourrait dire les groupes) auxquelles l’élément appartient.

Les valeurs de ```class``` ne sont pas définies par la spécification HTML. On tentera autant que possible d’indiquer des valeurs de ```class``` qui nous renseignent sur l’élément sur lequel elles sont appliquées.

```html
<button class="bouton--suppression grandes-marges avertissement">supprimer les fichiers</a>
```

> Le principal usage des classes est de permettre la sélection, en CSS, des éléments, afin de pouvoir indiquer comment ceux-ci doivent apparaître.

##### attributs style

l’attribut ```style``` est un attribut global qui peut être utilisé pour associer des propriétés CSS de mise en forme directement dans le HTML. Cela constitue toutefois une entorse au principe de séparation des préoccupations. C'estquelque chose que l’on souhaite ardemment éviter, il est donc préférable d’avoir une très bonne justification pour insérer directement dans son code source des propriétés CSS via cet attribut.

#### attributs spécifiques

Chaque type d’élément peut avoir des attributs spécifiques lui permettant d’assurer son rôle. Ceux-ci peuvent être trouvé dans la documentation relative à l’élément.

On peut par exemple se rendre sur [la documentation de l’élément ```a```](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/a) et voir, entre autres, que cet élément possède 7 attributs possibles en plus des attributs globaux : ```download```, ```href```, ```hreflang```, ```referrerpolicy```, ```rel```, ```target``` et ```type```.

#### attributs aria-* et role

WAI-ARIA (*Web Accessibility Initiative - Accessible Rich Internet Applications*) est une spécification technique qui concerne l’accessibilité (par exemple par des non ou mal-voyants).

Les attributs ```aria-*``` et ```role``` permettent d’offrir des documents plus accessibles, mais offrent aussi une grammaire normalisée permettant de décrire lesdits documents qui peut être utilisée à la place de classes choisies arbitrairement.

https://www.w3.org/TR/html5/dom.html#wai-aria

#### attribut data-*

Il est possible de créer des attributs personnalisés en HTML, il faut toutefois les préfixer avec la chaîne de caractère ```data-``` pour respecter la spécification html5.

Le but des attributs ```data-*``` est de stocker des données réservées à la page ou l’application et pour lesquelles il n’existe pas d’attribut approprié.

```html
<li data-length="2m11s">Beyond The Sea</li>
```

> l’exemple ci-dessus pourrait être utilisé pour représenter une piste sur un album, avec l’utilisation d’un attribut ```data-*``` pour spécifier la durée de la piste.
