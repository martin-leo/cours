# Plan de cours

* objectifs
* contexte
* définition
* les composants du Langage
* le dom
* vérifier son html
* ressources essentielles




* origines, état actuel, rapport au CSS/JS
  * origines
    il s’agit de créer un système de documents hypertexte.
    le CSS est là pour l’aspect visuel, la mise en page, mais on traitre aujourd’hui de documents. On laisse la mise en page de côté, l’aspect visuel
  * aujourd’hui
    pages web, mais aussi livres numérique (epub), applications (electron, react native)
  * le stack HTML/CSS/JS, quoi fait quoi ? SoC : separation of concerns, séparation des préoccupations. C'estl’idée qu’un programme doit être séparées en différentes parties dont chacune va s’occuper de ses propres affaires. Ici on va séparer 3 choses : au HTML le soin de structurer et d’ajouter une charge sémantique au document, au CSS le soin de définir la mise en forme, et au JavaScript de gérer l’interactivité. Et par conséquent le HTML ne s’occupe pas plus que le JavaScript de la présentation, que CSS et JavaScript ne s’occupent de sémantique que le HTML et le CSS ne s’occupent d’interactions. Ça c’est le principe, et on veux/va essayer de s’y tenir autant que possible. On verra que dans la réalité, ces éléments ne sont pas absolument déconnectés.
* Le document, le HTML, le DOM
  Le HTML s’occupe donc de document. Prenons un document. Cette magnifique carte postale.
  division du document en parties. Problème de représentation.
  on pourrait prendre le texte brut, mais
*


attributs :

* attributs globaux

  * [liste](https://html.spec.whatwg.org/multipage/dom.html#global-attributes)
  * id : sert à attribuer un identifiant unique à un élément, permet de cibler spécifiquement une partie du document, par exemple dans un lien.
  * class : sert à indiquer des jetons descriptifs de la nature de l’élément
  * ...
* attributs spécifiques
  * par exemple l’attribut href sert à spécifier l’adresse vers laquelle va pointer un lien
* attributs data-
* attributs aria- et role

comment le langage évolue-t-il ?

les documentations

* whatwg
* W3C
* MDN
* HTML5 doctor
* sitepoint ?

* html : langage très permissif

## dump

   : texte brut, on perd beaucoup d’information. De plus on voudrait qu’un programme puisse lire ce programme, pour pouvoir le présenter correctement, selon le dispositif, et on ne veut pas tout mélanger.
  https://en.wikipedia.org/wiki/Document_Object_Model
*


## TODO

* mettre à jour la carte postale avec une emphase (genre en plus gras en rouge comme au feutre)
