# Vérifier son HTML

Différents outils permettent de vérifier son HTML.

## Le navigateur

Le navigateur permet d’afficher la source de la page parcourue, et peut afficher d’éventuelles erreurs dans le code source. Ce n’est pas un moyen d’une grande fiabilité pour opérer cette vérification, mais cela peut être une manière rapide de constater l’existence d’un problème.

## Le validateur du W3C

Le W3C met à disposition un validateur qui permet de vérifier la conformité de son HTML : [validator.w3.org](https://validator.w3.org).
