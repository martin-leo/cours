# Structure d’une page html

## Structure minimale

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>titre du document</title>
  </head>
  <body>
    contenu du document
  </body>
</html>
```

### Représentation sous forme d’arborescence

```
html ―┬― head ――▸ métadonnées
      │
      └― body ――▸ contenu du document
```

> Différents éditeurs de texte permettent très facilement d’ajouter cette structure à un document vide. Il n’est pas nécessaire de la connaître par coeur, toutefois elle est suffisament simple pour être remémorée facilement.

## doctype : la déclaration de type de document

La première ligne de la structure vue plus haut est ce qu’on appelle la déclaration de type de document. Celle-ci a été simplifié avec HTML5.

```html
<!DOCTYPE html>
```

La déclaration de type de document devrait être la première ligne de votre document.

## l’élément html

l’élément ```html``` représente la racine du document, il contient logiquement tout votre HTML.

Il a pour enfants les élément  ```head``` et  ```body```.

## l’élément head

l’élément ```head``` représente une collection de métadonnées relative au document.

```html
<head>
  <meta charset="utf-8">
  <title>titre du document</title>
</head>
```

Ces métadonnées peuvent être le titre du document, son auteur, sa description, une image d’illustration, le lien vers les feuilles de style pour la présentation ou encore le lien vers le compte de l’auteur sur réseau social donné.

Ici sont représentés l’élément ```title``` qui permet de spécifier le titre du document, et une balise ```meta``` dont l’attribut charset indique qu’elle sert à indiquer l’encodage de caractères utilisé pour le document.

Bien d’autres métadonnées peuvent être spécifiées ici.

> Il est possible d’en savoir plus sur les balises utilisables dans l’élément ```head``` en se référent [aux spécifications](https://html.spec.whatwg.org/multipage/semantics.html#document-metadata).

## l’élément body

l’élément ```body``` représente le contenu principal du document.
