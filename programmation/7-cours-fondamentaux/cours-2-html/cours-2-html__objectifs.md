# objectif du cours

## objectifs : compréhensions, apprentissage et connaissances

| Compréhensions à long terme (comprendre que...)  | Objectifs d’Apprentissage (être capable de...) | Connaissances Essentielles (savoir que...) |
| --- | --- | --- |
| **C-2.1** Les trois technologies piliers du web sont HTML, CSS et JavaScript | **OA-2.1.1** | **CE-2.1.1A**  |

HTML, CSS et JavaScript ont d’autres usages que la création de sites internet. De manière non exhaustive : livre numérique, applications, installations, jeu vidéo, édition, etc.

HTML et CSS sont *living standards*, des langages en constante évolution. Il est nécessaire de se tenir au courant si ce sont des outils que l’on utilise régulièrement, ou pour concevoir des sites internet.

Il existe plusieurs versions de JavaScript dont l’adoption se fait beaucoup plus graduellement. Il est utile de connaître la dernière version de JavaScript supportée dans un environnement pour lequel on programme.

Le séparation des préoccupations est un principe de conception qui veut que l’on cloisonne les différents composantes d’une entité afin que chaque partie se concentre sur ce qu’elle doit faire.

HTML, CSS et JavaScript obéissent au principe de séparation des préoccupations : HTML s’occupe de la structure du document, CSS de sa présentation, son aspect visuel, et JavaScript de l’interactivité.

Même si cela n’est pas toujours possible, on souhaite autant que faire se peut respecter la séparation des préoccupations et ne pas mélanger HTML, CSS et JavaScript.
