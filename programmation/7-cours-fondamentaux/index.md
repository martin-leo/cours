# fondamentaux pour la programmation et les technologies du Web

## objectifs de la série de cours

l’objectif de ces cours est de donner des connaissances, compétences, ainsi qu’un vocabulaire fondamentaux pour aborder la programmation.

Ces cours ne visent pas à remplacer ce que l’on peut trouver en ligne, mais visent à les compléter. Cela en donnant des fondamentaux trop souvent non abordés, et en fournissant des compléments à des informations trop souvent partielles.

## cours 1 : lire et écrire

* [objectifs](cours-1-lire-ecrire/objectifs.html)
* [comment un ordinateur lit-il un programme ?](cours-1-lire-ecrire/comment-un-ordinateur-lit-il-un-programme.html)
* [comment un humain lit-il un programme ?](cours-1-lire-ecrire/comment-un-humain-lit-il-un-programme.html)
* [documentation](cours-1-lire-ecrire/documentation.html)

## cours 2 : HTML

* [objectifs]()

## cours 3 : CSS

* [objectifs]()

## cours 4 : JavaScript et notions essentielles de programmation

* [objectifs]()

## cours 5 :

* [objectifs]()

## cours 6 :

* [objectifs]()

## cours 7 :

* [objectifs]()

## annexes

* [lexique](annexes/lexique.html)
